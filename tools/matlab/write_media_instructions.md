# Media Update

## Update the headset media

-  Plug in a headset via USB

   - The first time, it may prompt you that it is installing drivers.
   - After the first time, you will likely hear the pleasant windows tones indicating a device has been plugged in and is recognized :)

-  If it isn't already open, start Matlab
-  Add the path where CHAMI was installed on your computer from Matlab's command line, e.g., `>> addpath('C:\Program Files\Creare\CHAMI\src')`
-  Change Matlab's current directory to the location where the write_media.m file is located.
-  Run the write_media script from Matlab's command line, e.g., `>> write_media`

   - When it successfully completes updating, it will display `Finished copying files to headset` in Matlab's command window

-  The headset may be unplugged from USB at that point
-  Repeat for all the headsets

