%
% Author: VAL
% Written: 4/10/2019
%
% This file grabs all the sounds from the current directory
% are writes them on the CHA under EXAMPLE directory
% Iterates through structured list, transferring each to cha, then
% confirming each transfer.

% 
% Filename key: 
% Provide a filename key if your filenames do not meet the WAHTS
% requirements (http://tabsint.org/docs/developer-guide/protocol-dev.html)

%% Housekeeping
clear all
close all
clc

%%
% CHAMI should already be installed! add path to your src folder
addpath('C:\Program Files\Creare\CHAMI\src');

% Make sure CHAMI is on your path
if isempty(which('chami'))
    error('write_media:CHAMI_not_found',...
        ['chami.m not found.  Please check that the ', ...
        'CHAMI\\src directory is in your Matlab path.'])
else
    % ready to go!
end
% Set input and output directories
files = dir('*.wav');
outputDir = 'EXAMPLE'; % where on the cha, AND locally, to put updated files

% Create a folder to store the renamed media
if ~isfolder(outputDir)
    mkdir(outputDir)
else
    % ready
end

% Connect to CHA
cha = chami;
cha.open;
cha.mkdir(outputDir); % create main output directory on cha

%% Load Files onto cha
% 1) Check if a file is already on the CHA
% 2) If already there, delete from CHA
% 3) Add file to CHA

% Note that the CHA has a maximum name length (excluding extension) of 8
% characters, and that remaining characters will be truncated

% remove all files in directory if any
try
    oldFiles = cha.dir(outputDir);
    disp('Deleting old files');
    for iF=1:length(oldFiles)
        filename = [outputDir '/' oldFiles(iF).name];
        disp(filename);
        cha.delete_file(filename);
    end
catch
    % directory was empty, CHA returned error because it can't
    % handle empty directory requests
end
        
% iterate over the list of files
% for each file, calculate RMS and store it in the metadata, write the file
% to the CHA, check to make sure it is there.
tmp = struct;
for i = 1:length(files)
    file = files(i);
    cha_file = strcat(outputDir, '/', file.name);
    [wavData, fs] = audioread(file.name);
    fsOut = 24000;

    % Resample:
    wavData = resample(wavData, fsOut, fs);

    % Rescale to -3 dB down from full scale:
    maxWD = max(abs(wavData(:)));
    wavData = wavData ./ (maxWD * sqrt(2));

    %the CHA assumes an RMS value of 0.707 (e.g. sine wave of amplitude 1.0) when scaling the level, you must correct for this.
    levelRMS = sqrt(mean(wavData.^2)); %Actual RMS value of the wav file
    offsetdB = 20.*log10(0.707./levelRMS); %(inverse of) dB re 0.707
    offsetcB = sprintf('%d ',round(offsetdB.*10)); %offset in dB mulitplied by 10 (cB);
    audiowrite(cha_file, wavData, fsOut, 'Comment', offsetcB); %The CHA can check the "Comment" field in the wav file meta data and automatically apply the correct scaling; 

    % If CHA does not have this file (it shouldn't any more - we deleted all files), then add it
    % Throw error if a file did not copy over
    try 
        cha.write_file(cha_file, cha_file);
        disp(['success: ',cha_file])
    catch
        disp([cha_file, ' did not copy over successfully to CHA '])
    end
end
%% Close connection to CHA
cha.close;

disp('Finished copying files to headset')
