% Demo function that can be used with the TabsintResults processeor
%
%
function demo(tr)
    % tr is the variable I use to refer to the `TabsintResults` class passed around
    % you can use read-only properties `tr.results` and `tr.raw`
    % and you can read/write to properties `tr.responses` and `tr.output`
    %
    % `tr.output` is designed for users to utilize - feel free to put whatever you want there
    % this struct will be returned at the end of `runfunctions()`

    %tr.results = [];
    tr.output.name = 'demo';
    tr.output.id = [tr.results(:).id];  % get an array of all the result ids
end