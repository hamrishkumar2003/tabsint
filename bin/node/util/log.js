
/**
 * Build system log module
 * @module
 */
var log = module.exports = {};
var process = require('process');


/**
 * Format log messages depending on type
 * @param  {string | object} msg - log message to format
 * @returns {string} formatted message
 */
log.format = function(msg) {
  if (typeof(msg) === 'object') {
    msg = JSON.stringify(msg);
  }

  return msg;
};

/**
 * Log Debug
 * @param  {string | object} msg - message to log
 */
log.debug = function(msg) {
  if (process.env.npm_config_tabdebug || process.env.npm_config_tabverbose) {
    if (!msg) {
      return;
    }

    msg = log.format(msg);
    console.log('[DEBUG]: ' + msg);
  }
}

/**
 * Log info
 * @param  {string | object} msg - message to log
 */
log.info = function(msg) {
  if (!msg) {
    return;
  }

  msg = log.format(msg);
  console.log('[INFO]: ' + msg);
}


/**
 * Log warn
 * @param  {string | object} msg - message to log
 */
log.warn = function(msg) {

  if (!msg) {
    return;
  }

  msg = log.format(msg);
  console.log('[WARNING]: ' + msg);
}


/**
 * Log an error message
 * @param  {string | object} msg - msg to log
 * @param  {string | object} e - error message
 */
log.error = function(msg, e) {

  if (!msg) {
    return;
  }

  msg = log.format(msg);

  console.log('[ERROR]: ' + msg);

  if (e) {
    console.log(e);
  }

}
