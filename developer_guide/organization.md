# Repository Organization

Below is a brief overview of the TabSINT file structure.

## / Root Directory 

- `/bin`: Scripts and executables that help you develop and build the app
- `/config`: these include `.json` configuration files that can be used to customize your tabsint build
- `/custom_plugins`: custom built cordova plugins that are included into the tabsint build
- `/dist`: *(not versioned)* build files created after running `npm run release`
- `/docs`: project documentation - user guide and developer guide
- `/node_modules`: *(not versioned)* packages installed by npm
- `/plugins`: *(not versioned)* cordova installed plugins
- `/resources`: images and icons used by cordova in the app build process
- `/test`: unit test and e2e configuration and support files
- `/www`: the tabsint web application. This is the *web page* that gets packaged by cordova as a native mobile application.

### `/www` Directory

- `/www/bower_components`: app dependencies installed by [bower](https://bower.io)
- `/www/img`: image files used by the application
- `/www/res`: resource files, including demo protocols, built in videos, and built in wav files
- `/www/styles`: CSS stylesheets and fonts
- `/www/scripts`: javascript files and html
- `/www/tabsint_plugins`: *(not versioned)* tabsint plugins installed based on the configuration file during the build process
