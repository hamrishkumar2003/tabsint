// jshint ignore: start

"use strict";

import * as angular from "angular";
import * as _ from "lodash";
import "angular-mocks";
import "../../app";

beforeEach(angular.mock.module("tabsint"));

describe("Gain Service", function() {
  var gain, disk, devices, media;

  beforeEach(
    angular.mock.inject(function($injector, _gain_, _disk_, _devices_, _media_) {
      gain = _gain_;
      disk = _disk_;
      devices = _devices_;
      media = _media_;
    })
  );

  it("Tab-A default gain should be 4.5272", function() {
    devices.model = "SamsungTabA";
    gain.reset();
    expect(disk.tabletGain).toEqual(4.5272);
  });

  it("Tab-A default gain with alternate name should be 4.5272", function() {
    devices.model = "SM-T380";
    gain.reset();
    expect(disk.tabletGain).toEqual(4.5272);
  });

  it("Tab-E default gain should be 0", function() {
    devices.model = "SamsungTabE";
    gain.reset();
    expect(disk.tabletGain).toEqual(0);
  });

  it("Tab E default gain with alternate name should be 0", function() {
    devices.model = "SAMSUNG-SM-T377A";
    gain.reset();
    expect(disk.tabletGain).toEqual(0);
  });

  it("Nexus 7 default gain should be 8.5868", function() {
    devices.model = "Nexus 7";
    gain.reset();
    expect(disk.tabletGain).toEqual(8.5868);
  });

  it("Unknown device default gain should be 0", function() {
    devices.model = "someRandomTablet";
    gain.reset();
    expect(disk.tabletGain).toEqual(0);
  });

  it("Wavfile calibrated w/ Nexus 7 zero-pt should have use old gain values (Tab-E)", function() {
    let calibration = {
      refType: "as-recorded",
      RMSC: 0.029645245348711016,
      realWorldRMSA: 0.29536161146394263,
      RMSA: 0.026264638391216535,
      wavRMSC: 0.12978618785231966,
      wavRMSA: 0.11498596999346107,
      calibrationFilter: "full",
      wavRMSZ: 0.1386851913593069,
      normFactor: 4.3779765127821015,
      RMSZ: 0.03167792037129402,
      realWorldRMSZ: 0.3562372140566327,
      realWorldRMSC: 0.33337856428921764,
      scaleFactor: 0.13519823071552697,
      tablet: "Nexus 7"
    };
    devices.model = "SamsungTabE";
    expect(gain.getTabletGain(calibration)).toEqual(-8.5868);
  });
  it("Wavfile calibrated w/ Nexus 7 zero-pt should have use old gain values (Nexus 7)", function() {
    let calibration = {
      refType: "as-recorded",
      RMSC: 0.029645245348711016,
      realWorldRMSA: 0.29536161146394263,
      RMSA: 0.026264638391216535,
      wavRMSC: 0.12978618785231966,
      wavRMSA: 0.11498596999346107,
      calibrationFilter: "full",
      wavRMSZ: 0.1386851913593069,
      normFactor: 4.3779765127821015,
      RMSZ: 0.03167792037129402,
      realWorldRMSZ: 0.3562372140566327,
      realWorldRMSC: 0.33337856428921764,
      scaleFactor: 0.13519823071552697,
      tablet: "Nexus 7"
    };
    devices.model = "Nexus 7";
    expect(gain.getTabletGain(calibration)).toEqual(0);
  });

  it("Wavfile calibrated w/ Nexus 7 zero-pt should have use old gain values (Tab-A)", function() {
    let calibration = {
      refType: "as-recorded",
      RMSC: 0.029645245348711016,
      realWorldRMSA: 0.29536161146394263,
      RMSA: 0.026264638391216535,
      wavRMSC: 0.12978618785231966,
      wavRMSA: 0.11498596999346107,
      calibrationFilter: "full",
      wavRMSZ: 0.1386851913593069,
      normFactor: 4.3779765127821015,
      RMSZ: 0.03167792037129402,
      realWorldRMSZ: 0.3562372140566327,
      realWorldRMSC: 0.33337856428921764,
      scaleFactor: 0.13519823071552697,
      tablet: "Nexus 7"
    };
    devices.model = "SamsungTabA";
    expect(gain.getTabletGain(calibration)).toEqual(-4.0596);
  });
  it("Wavfile calibrated w/ Nexus 7 zero-pt should have use old gain values (Tab-E)", function() {
    let calibration = {
      refType: "as-recorded",
      RMSC: 0.029645245348711016,
      realWorldRMSA: 0.29536161146394263,
      RMSA: 0.026264638391216535,
      wavRMSC: 0.12978618785231966,
      wavRMSA: 0.11498596999346107,
      calibrationFilter: "full",
      wavRMSZ: 0.1386851913593069,
      normFactor: 4.3779765127821015,
      RMSZ: 0.03167792037129402,
      realWorldRMSZ: 0.3562372140566327,
      realWorldRMSC: 0.33337856428921764,
      scaleFactor: 0.13519823071552697,
      tablet: "TabE"
    };
    devices.model = "SamsungTabE";
    expect(gain.getTabletGain(calibration)).toEqual(0);
  });
  it("Wavfile calibrated w/ Nexus 7 zero-pt should have use old gain values (Nexus 7)", function() {
    let calibration = {
      refType: "as-recorded",
      RMSC: 0.029645245348711016,
      realWorldRMSA: 0.29536161146394263,
      RMSA: 0.026264638391216535,
      wavRMSC: 0.12978618785231966,
      wavRMSA: 0.11498596999346107,
      calibrationFilter: "full",
      wavRMSZ: 0.1386851913593069,
      normFactor: 4.3779765127821015,
      RMSZ: 0.03167792037129402,
      realWorldRMSZ: 0.3562372140566327,
      realWorldRMSC: 0.33337856428921764,
      scaleFactor: 0.13519823071552697,
      tablet: "TabE"
    };
    devices.model = "Nexus 7";
    expect(gain.getTabletGain(calibration)).toEqual(8.5868);
  });

  it("Wavfile calibrated w/ Nexus 7 zero-pt should have use old gain values (Tab-A)", function() {
    let calibration = {
      refType: "as-recorded",
      RMSC: 0.029645245348711016,
      realWorldRMSA: 0.29536161146394263,
      RMSA: 0.026264638391216535,
      wavRMSC: 0.12978618785231966,
      wavRMSA: 0.11498596999346107,
      calibrationFilter: "full",
      wavRMSZ: 0.1386851913593069,
      normFactor: 4.3779765127821015,
      RMSZ: 0.03167792037129402,
      realWorldRMSZ: 0.3562372140566327,
      realWorldRMSC: 0.33337856428921764,
      scaleFactor: 0.13519823071552697,
      tablet: "TabE"
    };
    devices.model = "SamsungTabA";
    expect(gain.getTabletGain(calibration)).toEqual(4.5272);
  });
});
