"use strict";

import * as angular from "angular";
import "angular-mocks";
import "../../app";

beforeEach(angular.mock.module("tabsint"));

describe("Svantek", function() {
  var svantek;
  beforeEach(
    angular.mock.inject(function(_svantek_) {
      svantek = _svantek_;
    })
  );

  it("should load the svantek factory", function() {
    expect(svantek).toBeDefined();
  });
});
