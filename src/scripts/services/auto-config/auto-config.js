/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";
import * as _ from "lodash";

angular
  .module("tabsint.services.auto-config", [])
  .factory("autoConfig", function(
    $q,
    $timeout,
    cha,
    config,
    disk,
    examLogic,
    file,
    gettextCatalog,
    gitlab,
    localServer,
    logger,
    notifications,
    paths,
    protocol,
    tabsintServer,
    tasks
  ) {
    var api = {
      load: undefined,
      loadProtocol: undefined,
      loadExamView: undefined,
      add: undefined,
      checkMedia: undefined,
      pullMedia: undefined,
      androidIntentCallback: undefined
    };

    /**
     * Auto-configure tabsint and start protocol on QR Config Code scan
     * @returns {promise}
     */
    api.load = function() {
      tasks.register("autoconfig", "Loading Protocol");
      api.loadProtocol().finally(function() {
        tasks.deregister("autoconfig");
      });
    };

    /**
     * Callback from Android Intent
     */
    api.androidIntentCallback = function(onNewIntent) {
      logger.debug("exam.androidIntentCallback invoked, new Intent: " + JSON.stringify(onNewIntent));
      var tabsintConfig = onNewIntent["tabsintConfig"];
      // set headset connection and save data to disk here
      disk.cha.myCha = onNewIntent["headsetId"]; //"CHA (Btle): " +
      disk.audhere = onNewIntent["audhereResults"]; // putting this on disk so it's available for the duration of the protocol
      $timeout(cha.connect(), 1000);
      return $q
        .resolve() // Start the promise chain
        .then(function() {
          return config.load(tabsintConfig, true);
        }) // Load the configuration Qr Code into the config object
        .then(api.load); // Act on the config object: apply admin settings, load protocol and exam view
    };

    /**
     * Load protocol based on Qr Code scan protocol object.
     */
    api.loadProtocol = function() {
      disk.validateProtocols = config.validateProtocols;

      function loadProtocolAndSwitchToExamView() {
        return api
          .add()
          .then(function(gProtocol) {
            return protocol.load(gProtocol, disk.validateProtocols, false);
          })
          .then(function() {
            return examLogic.reset();
          })
          .then(function() {
            return examLogic.switchToExamView();
          })
          .catch(function(e) {
            $q.reject("Failed to load protocol and switch to exam view. " + e);
          });
      }

      if (config.server === "tabsintServer") {
        disk.servers.tabsintServer.url = config.tabsintServer.url;
        disk.servers.tabsintServer.username = config.tabsintServer.username;
        disk.servers.tabsintServer.password = config.tabsintServer.password;
        disk.servers.tabsintServer.site = config.tabsintServer.site;

        tabsintServer
          .reauthorize()
          .then(function() {
            if (tabsintServer.dm.authorized === false) {
              notifications.alert("TabSINT server authentication failed. Please verify credentials and try again.");
              logger.error(
                "TabSINT server authentication failed while auto-configuring TabSINT and loading protocol. "
              );
              return $q.reject();
            }
          })
          .catch(function(e) {
            logger.error("Failure while authentication TabSINT server.");
          });
        return loadProtocolAndSwitchToExamView();
      } else if (config.server === "gitlab") {
        /* 
          If build is heartrack, load the requested protocol
          Else, proceed as normal
        */

        if (disk.audhere) {
          var pName = disk.servers.gitlab.repository;
          var pPath;
          // Loop through loaded protocols to find the path
          for (const [key, value] of Object.entries(disk.protocols)) {
            console.log(`${key}: ${value}`);
            if (value.name == pName) {
              pPath = value.path;
            }
          }
          // Load the gitlab protocol that is already loaded into tabsint
          var protocolMeta = {
            name: pName,
            path: pPath,
            server: "gitlab",
            admin: false
          };
          return protocol
            .load(protocolMeta, disk.validateProtocols, false)
            .then(function() {
              return examLogic.reset();
            })
            .then(function() {
              return examLogic.switchToExamView();
            })
            .catch(function(e) {
              $q.reject("Failed to load protocol and switch to exam view. " + e);
            });
        } else {
          disk.servers.gitlab.repository = config.gitlab.repository;
          disk.servers.gitlab.version = config.gitlab.version;
          disk.servers.gitlab.host = config.gitlab.host;
          disk.servers.gitlab.token = config.gitlab.token;
          disk.servers.gitlab.group = config.gitlab.group;
          disk.gitlab.useTagsOnly = config.gitlab.onlyTrackTags;
          disk.servers.gitlab.resultsGroup = config.gitlab.resultsGroup;
          disk.servers.gitlab.resultsRepo = config.gitlab.resultsRepo;
          return loadProtocolAndSwitchToExamView();
        }
      } else if (config.server === "localServer") {
        disk.servers.localServer.protocolDir = config.localServer.protocolDir;

        /* 
          If build is heartrack, load the requested protocol
          Else, launch android filechooser and proceed as normal
        */

        if (disk.audhere) {
          var protocolMeta = {
            name: disk.servers.localServer.protocolDir,
            path:
              "file:///data/user/0/com.creare.skhr.tabsint/files/protocols/local/" +
              disk.servers.localServer.protocolDir +
              "/",
            server: "localServer",
            admin: false
          };
          return protocol
            .load(protocolMeta, disk.validateProtocols, false)
            .then(function() {
              return examLogic.reset();
            })
            .then(function() {
              return examLogic.switchToExamView();
            })
            .catch(function(e) {
              $q.reject("Failed to load protocol and switch to exam view. " + e);
            });
        } else {
          // require confirmation before launching filechooser
          notifications.alert(
            "Please select the " +
              disk.servers.localServer.protocolDir +
              " directory located in Documents/tabsint-protocols/",
            function() {
              return api
                .add()
                .then(function(gProtocol) {
                  return protocol.load(gProtocol, disk.validateProtocols, false);
                })
                .then(function() {
                  return examLogic.reset();
                })
                .then(function() {
                  return examLogic.switchToExamView();
                })
                .catch(function(e) {
                  $q.reject("Failed to load protocol and switch to exam view. " + e);
                })
                .finally(function() {
                  tasks.deregister("autoconfig");
                });
            }
          );
        }
      }
    };

    /**
     * Add protocols from server set on `disk.server`
     */
    api.add = function() {
      if (disk.server === "tabsintServer") {
        tasks.register("addTabsintServerProtocol", "Add TabSINT Server protocol");
        // if no site is defined, skip auto-configuration and reject to skip protocol loading
        if (!disk.servers.tabsintServer.site) {
          logger.warn("no site defined during auto-configuration");
          tasks.deregister("addTabsintServerProtocol");
          return $q.reject();
        }

        return tabsintServer
          .updateConfiguration(disk.servers.tabsintServer.site) // run site configuration update.
          .then(function(newProtocol) {
            api.checkMedia(_.last(disk.protocols));
            return newProtocol;
          })
          .finally(function() {
            tasks.deregister("addTabsintServerProtocol");
          });
      } else if (disk.server === "gitlab") {
        tasks.register("addGitlabProtocol", "Add Gitlab protocol");
        // if no repository is defined, skip auto-configuration, and reject to skip protocol loading
        if (!disk.servers.gitlab.repository) {
          logger.warn("no repository defined during auto-configuration");
          tasks.deregister("addGitlabProtocol");
          return $q.reject();
        }

        var host, group, token;
        host = disk.servers.gitlab.host;
        group = disk.servers.gitlab.group;
        token = disk.servers.gitlab.token;

        return gitlab
          .add(host, group, disk.servers.gitlab.repository, token, disk.servers.gitlab.version, "protocol")
          .then(function(repo) {
            // store on disk in the correct format
            var gProtocol = gitlab.defineProtocol(repo);
            protocol.store(gProtocol);

            // check for media repositories
            api.checkMedia(_.last(disk.protocols));

            // show user success message
            notifications.alert(
              `${gettextCatalog.getString("Successfully added protocol:")} ${repo.path_with_namespace}`
            );
            return gProtocol;
          })
          .catch(function(e) {
            if (e && e.msg) {
              notifications.alert(e.msg);
            } else {
              logger.error(
                `Unknown failure while adding gitlab protocol ${host} ${group} ${
                  disk.servers.gitlab.repository
                } ${token} ${disk.servers.gitlab.version} with error: ${angular.toJson(e)}`
              );
              notifications.alert(
                gettextCatalog.getString(
                  "TabSINT encountered an issue while accessing gitlab. Please verify the wifi connection and gitlab settings. Please upload your logs if the issue persists."
                )
              );
            }
          })
          .finally(function() {
            tasks.deregister("addGitlabProtocol");
          });
      } else if (disk.server === "localServer") {
        return tasks
          .register("Add local protocol", "Adding protocol...")
          .then(localServer.addProtocol)
          .finally(function() {
            return tasks.deregister("Add local protocol");
          });
      }
    };

    /**
     * Local function to check protocol and update media
     * Check current protocol, if it uses a common media repo, update if loaded/download if not loaded
     */
    api.checkMedia = function(meta) {
      return file
        .readFile(meta.path + "protocol.json")
        .then(function(p) {
          var deferred = $q.defer();
          // if the protocol uses a common media repo, we need to ensure the repo is loaded and up to date
          if (p && p.commonMediaRepository) {
            var midx = _.findIndex(disk.mediaRepos, {
              name: p.commonMediaRepository
            });

            // Protocol uses a common repo, which is currently checked out.  Check for updates for the common media repo
            if (midx > -1) {
              var mediaObject = disk.mediaRepos[midx];
              logger.debug(
                `This protocol uses the common media repository ${
                  mediaObject.name
                }. Checking for updates for the media repository now.`
              );
              return api.pullMedia(mediaObject);
            }

            // Protocol uses a common repo, but the common repo is not checked out - try now.
            else if (midx === -1) {
              logger.debug(
                `This protocol uses a common media repository ${
                  p.commonMediaRepository
                }, but the repository is not downloaded. Attempting to download now.`
              );

              // select location for protocols
              var host, group, token;
              host = disk.servers.gitlab.host;
              group = disk.servers.gitlab.group;
              token = disk.servers.gitlab.token;

              // download the latest version of the media repository
              return gitlab
                .add(host, group, p.commonMediaRepository, token, undefined, "media")
                .then(function(repo) {
                  api.storeMedia(gitlab.defineProtocol(repo));

                  // show user success message
                  notifications.alert(
                    `${gettextCatalog.getString("Successfully added media:")} ${repo.path_with_namespace}`
                  );
                })
                .catch(function(e) {
                  if (e && e.msg) {
                    notifications.alert(e.msg);
                  } else {
                    logger.error(
                      `Unknown failure while adding gitlab media ${host} ${group} ${
                        p.commonMediaRepository
                      } ${token} with error: ${angular.toJson(e)}`
                    );
                    notifications.alert(
                      gettextCatalog.getString(
                        "TabSINT encountered an issue while accessing gitlab. Please verify the wifi connection and gitlab settings. Please upload your logs if the issue persists."
                      )
                    );
                  }
                });
            }
          } else {
            deferred.resolve();
          }
        })
        .catch(function(err) {
          logger.error("Error while checking media: " + err);
        });
    };

    /**
     * Convienence function to pull media via gitlab and update disk media repo object
     * @param  {object} mediaObject - media object to pull
     */
    api.pullMedia = function(mediaObject) {
      return gitlab
        .pull(mediaObject.repo)
        .then(function(repo) {
          var pidx = _.findIndex(disk.mediaRepos, {
            path: paths.data(paths.gitlab(repo))
          });
          if (pidx !== -1) {
            disk.mediaRepos[pidx] = gitlab.defineProtocol(repo);
          }
        })
        .catch(function(e) {
          if (e && e.msg) {
            notifications.alert(e.msg);
          } else {
            logger.error(
              `Unknown failure while pulling gitlab protocol ${mediaObject.repo.host} ${mediaObject.repo.group} ${
                mediaObject.repo.name
              } ${mediaObject.repo.token} with error: ${angular.toJson(e)}`
            );
            notifications.alert(
              gettextCatalog.getString(
                "TabSINT encountered an issue while updating the media repository.  Please verify the media location. Please upload your logs if the issue persists."
              )
            );
          }
        });
    };

    return api;
  });
