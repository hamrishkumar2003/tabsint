/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

/* jshint bitwise: false */
/* globals ChaWrap: false */

"use strict";

import * as angular from "angular";
import * as _ from "lodash";

angular
  .module("tabsint.services.cha.exams", [])
  .factory("chaExams", function($q, $timeout, page, cha, chaResults, chaStreaming, logger, media, notifications) {
    var api = {
      polling: undefined,
      cancelPolling: undefined,
      setup: undefined,
      wait: undefined,
      reset: undefined,
      getChaInfo: undefined,
      state: undefined,
      storage: {},
      clearStorage: undefined,
      examType: undefined,
      complexExamType: undefined,
      examProperties: undefined,
      runTOBifCalledFor: {},
      playsoundInterval: undefined
    };

    // The storage field provides storage for the current state in a complex exam
    // The storage field is cleared using clearStorage on certain events using the plugins onEvent api
    api.clearStorage = function() {
      api.storage = {};
    };
    var exams = {
      AcceleratedThreshold: {},
      FrequencyPattern: {},
      HughsonWestlake: {},
      BekesyLike: {},
      BHAFT: {},
      BekesyMLD: {},
      ThreeDigit: {},
      DichoticDigits: {},
      MLD: {},
      HINT: {},
      ThirdOctaveBands: {},
      ToneGeneration: {},
      DPOAE: {},
      GAP: {},
      CalibrationCheck: {},
      TRT: {},
      PlaysoundArray: {},
      MaskedThreshold: {}
    };

    // Exam Reset Functions
    api.reset = function() {
      api.examType = undefined;
      api.examProperties = {};
      api.complexExamType = undefined;
      return api.resetPage();
    };

    api.resetPage = function() {
      if (angular.isDefined(page.dm)) {
        page.dm.hideProgressbar = true;
      }

      api.cancelPolling();
      return cha
        .abortExams()
        .then(cha.stopNoiseFeature)
        .then(chaStreaming.disconnectA2DP);
    };

    /**
     * Generic setup function for CHA exams. Stores examType and properties on this service.
     * If this exam type has its own setup function, run it using examProp.
     * @function
     * @param {string} examType - String from list defined in "exams" above.
                                  Source fields in protocol include:
                                  responseArea.type (chaHughsonWestlake, chaBekesyLike, chaBekesyMLD)
                                  audiometryType (top-level page property) (chaManualAudiometry, chaManualScreener) Defaults to hughsonWestlake.
                                  chaExams.complexExamType, which is same as responseArea.type.
                                  Also can be "PlaysoundArray", which is hardcoded.                      
     * @param {Object} examProp - Dictionary of properties defined in protocol as responseArea.examProperties
     */
    api.setup = function(examType, examProp) {
      api.examType = examType;
      var properties = examProp || {};

      // must convert to an examProperties object that can be sent to the CHA
      if (exams[api.examType].hasOwnProperty("setup")) {
        api.examProperties = exams[api.examType].setup(properties) || {};
      } else {
        api.examProperties = properties;
      }
    };

    /**
     * Calcuate the start level based on the protocol exam property "dynamicStartlevel" object.
     *
     * @param {*} dynStartLevel the "dynamicStartLevel" from the protocol. See the schema for details.
     * @param {*} Lstart the default start level specified in the protocol exam properties. A default value (currently 40) is used if none is provided by the protocol.
     */
    function calculateDynamicStartLevel(dynStartLevel, Lstart) {
      var base, offset, ret;
      var idList = dynStartLevel.baseIdList;

      // Get the list of results for the given page id(s), ensuring they are of type "HughsonWestlake".
      // Note: per the getPastResults function comment: this returns the results in the same order of the baseId's.
      // In other words, [id1, id2, id3, ...] should return [result_1_fromid_1, result_2_fromid_1, result_1_fromid_2, result_1_fromid_3. ...]
      var resultsList = chaResults.getPastResults(idList);

      // Get the "most recent" threshold result -> the highest index result of type threshold for the highest index id of dynamicStartLevel.baseIds.
      for (var i = resultsList.length - 1; i >= 0; i--) {
        if (resultsList[i].ResultType === "Threshold") {
          base = resultsList[i].Threshold;
          break;
        }
      }

      if (angular.isUndefined(base)) {
        ret = angular.isDefined(Lstart) ? Lstart : 40;
      } else {
        let minStartLevel = dynStartLevel.minStartLevel || 0;
        let maxStartLevel = dynStartLevel.maxStartLevel || 60;
        offset = angular.isDefined(dynStartLevel.offset) ? dynStartLevel.offset : 15;
        // dynamicStartLevel must be between 0 and 60.
        ret = Math.min(Math.max(minStartLevel, base + offset), maxStartLevel);
      }

      //console.log('newLstart: '+ret+', from offset: '+offset+', base: '+base);

      return ret;
    }

    exams.FrequencyPattern = {
      setup: function(examProp) {
        if (angular.isUndefined(examProp.NumberOfPresentations)) {
          examProp.NumberOfPresentations = 30;
        }
        return examProp;
      }
    };

    exams.ThirdOctaveBands = {
      setup: function(examProp) {
        if (page.dm.responseArea.measureBothEars || angular.isUndefined(examProp.InputChannel)) {
          // go to default left input channel
          examProp.InputChannel = "SMICR0";
        }
        return examProp;
      }
    };

    exams.DPOAE = {
      setup: function(examProp) {
        if (examProp.F2 && !examProp.F1) {
          examProp.F1 = examProp.F2 / 1.2; // F2 = 1.2*F1
        }
        return examProp;
      }
    };

    exams.MLD = {
      setup: function(examProp) {
        if (angular.isUndefined(examProp.UseSoftwareButton)) {
          examProp.UseSoftwareButton = true;
        }
        if (angular.isUndefined(examProp.Frequency)) {
          examProp.Frequency = 500;
        }
        if (angular.isUndefined(examProp.FixedLevel)) {
          examProp.FixedLevel = 70;
        }
        if (angular.isUndefined(examProp.ReferenceSignalEar)) {
          examProp.ReferenceSignalEar = 2;
        }
        return examProp;
      }
    };

    exams.GAP = {
      setup: function(examProp) {
        if (angular.isUndefined(examProp.UseSoftwareButton)) {
          examProp.UseSoftwareButton = 1;
        } else {
          examProp.UseSoftwareButton = examProp.UseSoftwareButton ? 1 : 0;
        }
        return examProp;
      }
    };

    exams.CalibrationCheck = {
      setup: function(examProperties) {
        examProperties.Tol_cal = 10;
        return examProperties;
      }
    };

    exams.TRT = {
      setup: function(examProp) {
        examProp.LevelUnits = angular.isDefined(examProp.LevelUnits) ? examProp.LevelUnits : "dB HL";

        examProp.ThresholdLevels = [];
        examProp.Ears = [];
        examProp.Frequencies = [];
        examProp.Thresholds.forEach(function(item) {
          examProp.ThresholdLevels.push(item.ThresholdLevel);
          examProp.Ears.push(item.Ear);
          examProp.Frequencies.push(item.Frequency);
        });
        examProp.Ears = examProp.Ears.map(function(ear) {
          return ear === "Right" ? 1 : 0;
        });

        examProp.Thresholds = undefined;

        return examProp;
      }
    };

    exams.MaskedThreshold = {
      setup: function(examProp) {
        if (angular.isUndefined(examProp.F)) {
          examProp.F = 1000;
        }
        return examProp;
      }
    };

    exams.BHAFT = {
      setup: function(examProp) {
        if (angular.isUndefined(examProp.Fstart)) {
          examProp.Fstart = 1000;
        }
        if (angular.isUndefined(examProp.OutputChannel)) {
          examProp.OutputChannel = "HPL0";
        }
        // BHAFT doesnt have propery .Level ---> it has .L0 according to .json. But that throws error so using level.
        if (angular.isUndefined(examProp.Level)) {
          examProp.Level = 80;
        }
        return examProp;
      }
    };
    /*
      NOTE: DynamicStartLevel supported for chaBekesyLike, chaHughsonWestlake, and chaBekesyMLD response areas
      since all corresponding schemas support audiometryLevelProperties (where DynamicStartLevel defn. lives)
      NOTE: chaExams.setup (which calls the below functions) is invoked in src/scripts/components/response-areas/cha-response-areas.js#63
      examType argument comes from responseArea "type" as defined in protocol, though "cha" prefix is dropped.
      TODO: Consolidate these setup functions.
    */
    exams.BekesyLike = {
      setup: function(examProp) {
        // Dynamic Start Level
        if (examProp.DynamicStartLevel) {
          examProp.Lstart = calculateDynamicStartLevel(examProp.DynamicStartLevel, examProp.Lstart);
          delete examProp.DynamicStartLevel;
        }
        if (angular.isUndefined(examProp.F)) {
          examProp.F = 1000;
        }
        if (angular.isUndefined(examProp.OutputChannel)) {
          examProp.OutputChannel = "HPL0";
        }
        return examProp;
      }
    };

    exams.BekesyMLD = {
      setup: function(examProp) {
        // Dynamic Start Level
        if (examProp.DynamicStartLevel) {
          examProp.Lstart = calculateDynamicStartLevel(examProp.DynamicStartLevel, examProp.Lstart);
          delete examProp.DynamicStartLevel;
        }
        if (angular.isUndefined(examProp.F)) {
          examProp.F = 1000;
        }
        if (angular.isUndefined(examProp.OutputChannel)) {
          examProp.OutputChannel = "HPL0";
        }
        return examProp;
      }
    };

    /**
     * Setup method for HughsonWestlake exams.
     *
     * The dynamicStartLevel allows each page with a HughsonWestlake exam to adjust its start level
     * based on the threshold of a previous exam, plus an offset.
     */
    exams.AcceleratedThreshold = {
      setup: function(examProp) {
        if (angular.isUndefined(examProp.F)) {
          examProp.F = 1000;
        }
        if (angular.isUndefined(examProp.OutputChannel)) {
          examProp.OutputChannel = "HPL0";
        }
        return examProp;
      }
    };

    /**
     * Setup method for HughsonWestlake exams.
     *
     * The dynamicStartLevel allows each page with a HughsonWestlake exam to adjust its start level
     * based on the threshold of a previous exam, plus an offset.
     */
    exams.HughsonWestlake = {
      setup: function(examProp) {
        // Dynamic Start Level
        if (examProp.DynamicStartLevel) {
          examProp.Lstart = calculateDynamicStartLevel(examProp.DynamicStartLevel, examProp.Lstart);
          delete examProp.DynamicStartLevel;
        }

        // Relative Frequency
        if (examProp.RelativeF) {
          examProp.F = calculateRelativeF(examProp.F, examProp.RelativeF);
          delete examProp.RelativeF;
        }

        if (angular.isUndefined(examProp.StepSize)) {
          examProp.StepSize = 5;
        }

        if (angular.isUndefined(examProp.F)) {
          examProp.F = 1000;
        }
        if (angular.isUndefined(examProp.OutputChannel)) {
          examProp.OutputChannel = "HPL0";
        }
        return examProp;

        function calculateRelativeF(F, rel) {
          var LUT, i;
          if (typeof rel[1] !== "number" || typeof rel[2] !== "number") {
            logger.warn(
              "Numerator or Denominator in relative frequency conditions are not specified as numbers, returning un-modified HAF"
            );
            return F;
          }
          if (angular.isDefined(rel[3])) {
            // protocol defined a look up table - use that
            if (page.dm.lookUpTables) {
              _.forEach(page.dm.lookUpTables, function(lut) {
                if (lut.name === rel[3]) {
                  LUT = lut.table;
                }
              });
            }

            if (angular.isUndefined(LUT)) {
              return F;
            }
            var lutInd = LUT.indexOf(F);

            if (rel[0] === "below") {
              if (lutInd < 0) {
                for (i = 0; i < LUT.length; i++) {
                  if (LUT[i] > F) {
                    lutInd = i;
                    break;
                  }
                }
              }
              if (lutInd - rel[1] >= 0) {
                return LUT[lutInd - rel[1]];
              } else {
                logger.warn(
                  "CHA calculateRelativeF reached lowest value in look up table.  HAF = " +
                    F +
                    ", index = " +
                    lutInd +
                    ", step = " +
                    rel[1]
                );
                return LUT[0];
              }
            } else if (rel[0] === "above") {
              if (lutInd < 0) {
                for (i = LUT.length; i >= 0; i--) {
                  if (LUT[i] < F) {
                    lutInd = i;
                    break;
                  }
                }
              }
              if (lutInd + rel[1] < LUT.length) {
                return LUT[lutInd + rel[1]];
              } else {
                logger.warn(
                  "CHA calculateRelativeF reached highest value in look up table.  HAF = " +
                    F +
                    ", index = " +
                    lutInd +
                    ", step = " +
                    rel[1]
                );
                return LUT[LUT.length - 1];
              }
            }
          } else {
            // no lookup table provided, use math
            if (rel[0] === "below") {
              return Math.round(F * Math.pow(2, -rel[1] / rel[2]));
            } else if (rel[0] === "above") {
              return Math.round(F * Math.pow(2, rel[1] / rel[2]));
            }
          }
        }
      }
    };

    api.runTOBifCalledFor = function() {
      var deferred = $q.defer();
      var measure = page.dm.responseArea.measureBackground;
      if (measure !== angular.undefined && measure === "ThirdOctaveBands") {
        logger.debug("Measuring background using " + measure);
        var tmpMainText = page.dm.questionMainText;
        var tmpSubText = page.dm.questionSubText;
        var tmpInstText = page.dm.instructionText;
        page.dm.questionMainText = "Measuring Background Noise";
        page.dm.questionSubText = "Please sit quietly and do not move.";
        page.dm.instructionText =
          "This test measures the background noise levels using third-octave bands.  Wait quietly for measurement to complete.";
        return cha
          .requestStatusBeforeExam()
          .then(function() {
            return cha.queueExam("ThirdOctaveBands", {});
          })
          .then(api.wait.forReadyState)
          .then(cha.requestResults)
          .then(function(results) {
            chaResults.addTOBResults(results);
            page.dm.questionMainText = tmpMainText;
            page.dm.questionSubText = tmpSubText;
            page.dm.instructionText = tmpInstText;
          });
      } else {
        //ThirdOctaveBands not called for
        deferred.resolve();
        return deferred.promise;
      }
    };

    api.getChaInfo = function() {
      if (cha.myCha.id) {
        return {
          serialNumber: cha.myCha.id.serialNumber,
          buildDateTime: cha.myCha.id.buildDateTime,
          probeId: cha.myCha.probeId,
          vBattery: cha.myCha.vBattery
        };
      } else {
        logger.warn(`Requesting CHA info, but no cha is connected`);
        return {};
      }
    };

    api.wait = {
      forReadyState: function(initialDelay, intervalDelay) {
        var deferredExam = $q.defer();

        if (angular.isUndefined(initialDelay)) {
          initialDelay = 500;
        }
        if (angular.isUndefined(intervalDelay)) {
          intervalDelay = 500;
        }
        logger.debug(
          "CHA - Beginning to poll status every " + intervalDelay + " ms, after a delay of " + initialDelay + " ms."
        );

        api.cancelPolling();
        $timeout(function() {
          api.polling = setInterval(function() {
            return cha.requestStatus().then(
              function(status) {
                if (status.State === 1) {
                  api.cancelPolling();
                  deferredExam.resolve();
                }
              },
              function(err) {
                clearInterval(api.polling);
                deferredExam.reject(err);
              }
            );
          }, intervalDelay);
        }, initialDelay);

        return deferredExam.promise;
      },

      forReadyState2: function(initialDelay, intervalDelay) {
        var deferredExam = $q.defer();
        var count = 0;

        if (angular.isUndefined(initialDelay)) {
          initialDelay = 500;
        }
        if (angular.isUndefined(intervalDelay)) {
          intervalDelay = 500;
        }
        console.log(
          "CHA - Beginning to poll status every " + intervalDelay + " ms, after a delay of " + initialDelay + " ms."
        );

        api.cancelPolling();
        $timeout(function() {
          api.polling = setInterval(function() {
            return cha.requestStatus().then(
              function(status) {
                console.log("status", status);
                if (status.State === 2) {
                  api.cancelPolling();
                  deferredExam.resolve();
                } else {
                  count += 1;
                  if (count >= 60) {
                    console.log("WAITED 30 seconds, canceling polling.");
                    api.cancelPolling();
                    deferredExam.resolve();
                  }
                }
              },
              function(err) {
                clearInterval(api.polling);
                deferredExam.reject(err);
              }
            );
          }, intervalDelay);
        }, initialDelay);

        return deferredExam.promise;
      },

      forStreamingState: function(initialDelay, intervalDelay) {
        if (angular.isUndefined(initialDelay)) {
          initialDelay = 0;
        }
        if (angular.isUndefined(intervalDelay)) {
          intervalDelay = 500;
        }
        logger.debug(
          "CHA - Beginning to poll status every " + intervalDelay + " ms, after a delay of " + initialDelay + " ms."
        );

        api.cancelPolling();
        setTimeout(() => {
          // Infinite loop of requestStatus and checkA2DPConnection to keep alive
          api.polling = setInterval(async () => {
            try {
              var status = await cha.requestStatus();
            } catch (e) {
              clearInterval(api.polling);
              return;
            }
            if (status.State === 1) {
              // TalkThrough exam over, stop polling
              api.cancelPolling();
            }
            try {
              await chaStreaming.checkA2DPConnection();
            } catch (e) {
              clearInterval(api.polling);
              api.cancelPolling();
              return;
            }
          }, intervalDelay);
        }, initialDelay);

        return;
      }
    };

    api.cancelPolling = function() {
      try {
        clearInterval(api.polling);
      } catch (e) {
        logger.debug("CHA - Failed to clear results polling in chaExams with error: " + JSON.stringify(e));
      }
    };

    /**
     * PlaySound Exam
     * @param  {array} chaWavFiles - Array of string or chaWavFiles objects (see *chaWavFiles* in `page.json` schema) describing sounds to play on the SD card of the WAHTS
     * @return {promise}           - promise to queue a PlaySound exam
     */
    api.playSound = function(chaWavFiles) {
      function checkPlaysoundIsDone() {
        cha
          .requestStatus()
          .then(function(status) {
            if (status.State === 2) {
              // still running
              api.state = "Playing";
            } else if (status.State !== 1) {
              api.state = "Unknown";
              return $q.reject({ code: 66, msg: "Cha is in an unknown state" });
            } else if (status.State === 1) {
              // done
              api.state = "Done";
              clearInterval(api.playsoundInterval);
              $q.resolve();
              return $q.promise;
            }
          })
          .catch(function(err) {
            clearInterval(api.playsoundInterval);
            cha.errorHandler.main(err);
          });
      }
      if (chaWavFiles && chaWavFiles.length > 0) {
        try {
          clearInterval(api.playsoundInterval);
          console.log("clearing interval success");
        } catch {
          console.log("clearing interval failed");
        }
        return cha
          .requestStatus()
          .then(function(status) {
            status.vBattery = Math.round(status.vBattery * 100) / 100;
            logger.debug("CHA - Status before queueing exam: " + angular.toJson(status));
            if (status.State === 2) {
              logger.warn("CHA exam is still running while user queues an exam. Aborting exams...");
              return cha.abortExams();
            } else if (status.State !== 1) {
              return $q.reject({
                code: 66,
                msg: "Cha is in an unknown state"
              });
            }
          })
          .then(function() {
            function processWav(wav) {
              // allow each member of chaWavFiles input to be a string path
              if (typeof wav === "string") {
                wav = {
                  SoundFileName: angular.copy(wav)
                };

                // this should always be caught by schema validation, but adding error handling in case someone gets here
              } else if (typeof wav !== "object") {
                notifications.alert(
                  'The "chaWavFiles" field of this page is invalid. Please validate this protocol against the protocol schema.'
                );
                logger.error("PlaySound wav file definition must be an object or a string: " + JSON.stringify(wav));
                return;
              }

              // set `path` to `SoundFileName`
              if (angular.isDefined(wav.path)) {
                wav.SoundFileName = wav.path;
              } else {
                wav.SoundFileName = ""; // this will throw an error on the CHA that the wav file cannot be found
              }

              // assume files that don't start with C: are in the C:USER/ directory
              if (!wav.SoundFileName.startsWith("C:")) {
                wav.SoundFileName = "C:USER/" + wav.SoundFileName;
              }

              // Leq default
              if (!wav.Leq) {
                // Leq default
                wav.Leq = [72, 72, 0, 0];
              } else if (wav.Leq.length === 2) {
                // handle 2 single inputs
                wav.Leq.concat(0, 0);
              }

              // UseMetaRMS default
              wav.UseMetaRMS = angular.isDefined(wav.UseMetaRMS)
                ? wav.UseMetaRMS
                : angular.isDefined(wav.useMetaRMS)
                ? wav.useMetaRMS
                : false;

              return wav;
            }

            // construct playSound exam object
            var wav = processWav(chaWavFiles[0]);

            // if processWav fails, reject
            if (!wav) {
              return $q.reject();
            }

            var pse = {
              UseMetaRMS: wav.UseMetaRMS, // old way - need to make sure nobody depends on this: wav.path.startsWith('C:USER/'),
              SoundFileName: wav.SoundFileName,
              Leq: wav.Leq
            };

            if (chaWavFiles.length === 2) {
              var wav2 = processWav(chaWavFiles[1]);

              // if processWav2 fails, reject
              if (!wav2) {
                return $q.reject();
              }

              pse.SecondSoundFileName = wav2.SoundFileName;
              pse.SecondLeq = wav2.Leq;
              // pse.SecondFileDelay = wav2.SecondFileDelay || 0;
            }

            return cha.queueExam("PlaySound", pse);
          })
          .then(function() {
            api.playsoundInterval = setInterval(checkPlaysoundIsDone, 500);
          }) // we request results right away to make sure there is not an error with the playing of the wav file, and make sure wave file is done playing
          .catch(function(e) {
            clearInterval(api.playsoundInterval);
            logger.error("CHA - playWavs failed with error: " + JSON.stringify(e));
          });
      }
    };

    api.startTalkThrough = async () => {
      logger.debug("CHA - starting talkThrough");

      await media.stopAudio();
      cha.requestStatus().then(function(status) {
        logger.debug("CHA - Status before queueing talkThrough: " + angular.toJson(status));
      });
      try {
        try {
          // Check if A2DP connection is active. If not, an error is thrown and an A2DP connection attempt is made.
          // await chaStreaming.connectA2DP();
          await chaStreaming.checkA2DPConnection();
        } catch (e) {
          await chaStreaming.connectA2DP();
        }

        // TalkThrough times out after 5 seconds so CHA doesn't get stuck in headset mode if connection dies.
        await cha.queueExam("TalkThrough", {});
        try {
          await api.wait.forStreamingState(500, 1000); // need to poll, to keep the TalkThrough alive.
        } catch (e) {
          var msg = "";
          logger.error("CHA - TalkThrough failed with error: " + JSON.stringify(e));
          if (e.code && e.code === 64) {
            msg =
              "Connection with the wireless headset was lost while streaming audio.  " +
              "The audio may start playing through the tablet speakers.  " +
              "Please hand the tablet to an administrator.";
          } else if (e.code && (e.code === 554 || e.code === 557)) {
            msg =
              "The tablet is not set up to stream to the headset. Please hand the tablet to an administrator. \n\nTo set up the streaming connection, try reconnecting to the headset.";
          } else if (e.code && (e.code === 558 || e.code === 555 || e.code === 560)) {
            msg = "The tablet failed to set up a streaming connection. Please hand the tablet to an administrator.";
          } else {
            msg =
              "The tablet was unable to initialize streaming to the WAHTS.  Please hand the tablet to an administrator.";
          }
          media.stopAudio();
          cha.abortExams();
          notifications.alert(msg);
        }
        // After A2DP connection established, check that headset in TalkThrough state.
        let status = await cha.requestStatus();
        logger.debug("CHA - Status after queuing talkThrough: " + angular.toJson(status));
        if (status.State !== 2) {
          logger.error("CHA - TalkThrough exam was not started");
          logger.error({
            code: -1,
            message: "Check protocol"
          });
          return;
        }
      } catch (e) {
        logger.debug("TalkThrough exam failed. Error was" + JSON.stringify(e));
      }
    };

    api.stopTalkThrough = function() {
      logger.debug("CHA - stopping talkThrough");
      media.stopAudio();
      api.reset();
    };

    return api;
  });
