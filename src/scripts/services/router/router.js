/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";

angular
  .module("tabsint.services.router", [])

  .factory("router", function(app, disk, authorize, plugins, paths, $window) {
    var router = {};

    // default home page is 'WELCOME'
    router.page = "WELCOME";

    /**
     * Go to a new page based on page string (i.e. 'ADMIN', 'WELCOME')
     * @param  {string} view - string view name
     * @param  {boolean} auth - whether to show the auth popup before allowing the user to transition to page
     */
    router.goto = function(view, auth) {
      app.ready().then(function() {
        if (view === "DOCS") {
          window.open("http://tabsint.org/docs/introduction", "_system");
        } else if (auth && !disk.debugMode) {
          authorize.modalAuthorize(function() {
            router.page = view;
          });
        } else {
          router.page = view;
        }
      });
    };

    return router;
  });
