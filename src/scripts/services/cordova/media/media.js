/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

/*global Media */

"use strict";

import * as angular from "angular";
import * as _ from "lodash";

angular
  .module("tabsint.services.cordova.media", [])

  .factory("media", function($timeout, disk, gain, gettextCatalog, logger, notifications, tabsintNative, devices) {
    var media = {
      instances: [],
      playAudio: undefined,
      stopAudio: undefined
    };

    media.calculateVolume = function(wavfile) {
      var inputFlag = false,
        specifiedPaRMS,
        waveformRMS,
        volume,
        level,
        method,
        weighting,
        calFactor;

      // TODO: included for backwards compatibility. remove in future version
      // Can this actually be removed now?
      if (typeof wavfile.targetSPL === "string") {
        wavfile.targetSPL = parseFloat(wavfile.targetSPL);
      }

      level = angular.isDefined(wavfile.targetSPL) ? wavfile.targetSPL : 65.0; // default to 65 dB.
      // when playing uncalibrated media, use default tablet gain for this tablet
      level = level + gain.getTabletGain(wavfile.cal); // tablet specific gain: measured jack output sound level difference from the Nexus 7
      method = wavfile.playbackMethod || "arbitrary"; // default to arbitrary.
      weighting = wavfile.weighting || "Z"; // default to Z weighting

      if (_.find(["A", "C", "Z"], weighting) && level) {
        inputFlag = true;
      }

      if (method === "arbitrary") {
        specifiedPaRMS = 20e-6 * Math.pow(10, level / 20.0);
        waveformRMS = specifiedPaRMS * wavfile.cal.scaleFactor;
        volume = waveformRMS / wavfile.cal["wavRMS" + weighting];
      } else if (method === "as-recorded") {
        if (wavfile.cal.refType !== "as-recorded") {
          notifications.alert(gettextCatalog.getString('Playback Error: Invalid "as-recorded" playback request.'));
          logger.error('Invalid "as-recorded" playback request.');
        }
        waveformRMS = wavfile.cal.realWorldRMSZ * wavfile.cal.scaleFactor;
        var volume_prior_tabletGain = waveformRMS / wavfile.cal.wavRMSZ;
        // If playing calibrated media, check what zero point was used (Nexus 7 or TabE) and
        // use appropriate gain.
        volume = Math.pow(10, (20 * Math.log10(volume_prior_tabletGain) + gain.getTabletGain(wavfile.cal)) / 20);
      } else if (method === "WRT-reference" && inputFlag) {
        if (wavfile.cal.refType !== "WRT-reference") {
          notifications.alert(gettextCatalog.getString('Playback Error: Invalid "WRT-reference" playback request.'));
          logger.error('Invalid "WRT-reference" playback request.');
        }
        specifiedPaRMS = 20e-6 * Math.pow(10, level / 20.0);
        calFactor = specifiedPaRMS / wavfile.cal["refRMS" + weighting];
        waveformRMS = calFactor * wavfile.cal.RMSZ * wavfile.cal.scaleFactor;
        volume = waveformRMS / wavfile.cal.wavRMSZ;
      } else {
        logger.error(`Invalid arguments to calculateVolume: ${angular.toJson(wavfile)}`);
        notifications.alert(gettextCatalog.getString("Playback Error: Invalid arguments on playback."));
      }

      var msg;
      if (volume > 1.0001) {
        msg =
          gettextCatalog.getString(
            "CAUTION: Wavfile requested at a volume > 1.0. Playing at 1.0, which does NOT meet spec. Requested volume: "
          ) + volume;
        logger.warn("" + msg);
        notifications.alert(msg + ".  " + gettextCatalog.getString("This message only appears in admin mode."));
        volume = 1.0;
      } else if (volume > 1.0) {
        // Catches a common floating-point rounding error case where volume is *slightly* greater than 1.0.
        // There's nothing wrong with this, it is expected.
        volume = 1.0;
      }

      if (0 <= volume && volume <= 1) {
        return volume;
      } else {
        msg = gettextCatalog.getString("Volume out of range: ") + volume;
        logger.error("" + msg);
        notifications.alert(msg);
      }
    };

    /**
     * Play a calibrated wavfile.
     * @param wavfile : as defined in protocol_schema.json.
     */
    media.playWav = function(wavfiles, startDelay, volumeLevelFlag) {
      startDelay = angular.isDefined(startDelay) ? startDelay : 1000;
      var wavList = [];
      _.forEach(wavfiles, function(wavfile) {
        if (angular.isDefined(wavfile.path)) {
          var volume;
          if (!wavfile.cal) {
            logger.warn("No calibration for wavfile " + wavfile.path + " ... playing at 25%.");
            volume = 0.25;
          } else {
            if (angular.isDefined(wavfile.targetSPL) && angular.isDefined(wavfile.targetSPL.length)) {
              volume = [];
              wavfile.targetStereoSPL = wavfile.targetSPL; //HG; Saving copy, overwritting below
              for (var i = 0; i < wavfile.targetStereoSPL.length; i++) {
                wavfile.targetSPL = wavfile.targetStereoSPL[i];
                volume.push(media.calculateVolume(wavfile));
              }
            } else {
              volume = media.calculateVolume(wavfile);
            }
          }
          wavList.push({
            src: wavfile.path,
            vol: volume,
            sTime: wavfile.startTime,
            eTime: wavfile.endTime
          });
        }
      });

      media.playAudio(wavList, startDelay, volumeLevelFlag);
    };

    /**
     * Container for media instances that are active.
     * Only 7 media instances can be active before android crashes
     */
    media.instances = [];

    /**
     * Play sound files immediately.
     *
     * @param  {[type]} srcArr - array of source objects with structure:
     *                         src : path relative to 'www' directory.
     *                         volume : 0.0 (silent) to 1.0 (full volume). Linear.
     *                         sTime: startTime, in milli-seconds
     *                         eTime: endTime, in milli-seconds
     */
    media.playAudio = function(srcArr, startDelay, volumeLevelFlag) {
      startDelay = angular.isDefined(startDelay) ? startDelay : 1000;

      if (angular.isUndefined(window.Media)) {
        logger.error('Cordova "Media" is unavailable');
        notifications.alert(gettextCatalog.getString('Cordova plugin "Media" is unavailable'));
        return;
      }

      //HG;9/1/20 if volLel not specified reset vol
      if (!volumeLevelFlag) {
        tabsintNative.resetAudio(null, tabsintNative.onVolumeErr);
      }

      _.forEach(srcArr, function(wav) {
        var mediaInstance;
        mediaInstance = new Media(
          wav.src,
          function() {
            mediaInstance.release();
            media.instances = _.reject(media.instances, function(inst) {
              return inst === mediaInstance;
            });
          },
          function(e) {
            logger.error("Error while playing audio: " + angular.toJson(e));
            mediaInstance.release();
            media.instances = _.reject(media.instances, function(inst) {
              return inst === mediaInstance;
            });
          }
        );

        //HG; TO SUPPORT BROWSER STEREO PLAYING
        if ((devices.platform.toLowerCase() === "browser") & (wav.vol.length > 1)) {
          mediaInstance.audioCtx = new (window.AudioContext || window.webkitAudioContext)();
          mediaInstance.oscillator = mediaInstance.audioCtx.createMediaElementSource(mediaInstance.node);
          mediaInstance.gainNodeL = mediaInstance.audioCtx.createGain();
          mediaInstance.gainNodeR = mediaInstance.audioCtx.createGain();
          mediaInstance.merger = mediaInstance.audioCtx.createChannelMerger(2);

          mediaInstance.oscillator.connect(mediaInstance.gainNodeL);
          mediaInstance.oscillator.connect(mediaInstance.gainNodeR);

          mediaInstance.gainNodeL.connect(mediaInstance.merger, 0, 0);
          mediaInstance.gainNodeR.connect(mediaInstance.merger, 0, 1);

          mediaInstance.merger.connect(mediaInstance.audioCtx.destination);
        }

        mediaInstance.setVolume(0);
        media.instances.push({
          instance: mediaInstance,
          volume: wav.vol,
          startTime: wav.sTime,
          endTime: wav.eTime
        }); // added by HG
      });

      $timeout(function() {
        _.forEach(media.instances, function(mediaItem) {
          //HG; Setting vol for LR only once
          if (mediaItem.volume.length != undefined) {
            // mediaItem.instance.setVolume(mediaItem.volume.reduce(function(a, b) {return Math.max(a, b)}));
            mediaItem.instance.setVolume(1); //setting vol to max then gain-LR is going to adjust , still a question mark if it should be one
          } else {
            mediaItem.instance.setVolume(mediaItem.volume);
          }
          mediaItem.instance.play();
          mediaItem.instance.setVolume(mediaItem.volume);

          if (mediaItem.startTime > 0) {
            // added by HG support seek to functionality
            mediaItem.instance.seekTo(mediaItem.startTime);
          }

          mediaItem.instance.setVolume(mediaItem.volume);

          if (mediaItem.endTime > 0 && mediaItem.endTime > mediaItem.startTime) {
            $timeout(function() {
              mediaItem.instance.pause();
            }, mediaItem.endTime - mediaItem.startTime);
          }

          logger.info("Playing " + angular.toJson(mediaItem.instance) + " at Volume " + mediaItem.volume.toString());

          tabsintNative.getAudioVolume(
            function(msg) {
              if (msg.volume < 99) {
                logger.debug("Tried volume " + mediaItem.volume + ", but system volume is now " + msg.volume + "%");
              }
            },
            function(e) {
              logger.error("Failed while checking tablet audio volume: " + e);
            }
          );
        });
      }, startDelay);
    };

    /**
     * Stop all media instances currently running
     */
    media.stopAudio = function() {
      if (media.instances.length > 0) {
        _.forEach(media.instances, function(item) {
          item.instance.getCurrentPosition(
            function(position) {
              if (position > 0) {
                item.instance.stop();
              }
              item.instance.release();
              media.instances = _.reject(media.instances, function(inst) {
                return inst === item;
              });
            },
            function(err) {
              logger.error("error getting pos: " + err);
              item.instance.release();
              media.instances = _.reject(media.instances, function(inst) {
                return inst === item;
              });
            }
          );
        });
      }
    };

    media.playPhrase = function() {
      media.stopAudio();
      media.playAudio([{ src: "/android_asset/www/res/wavs/word01_norm.wav", vol: 1.0 }]);
    };

    media.playCompAudio = function() {
      media.stopAudio();
      media.playAudio([{ src: "/android_asset/www/res/wavs/CompAudioTest.wav", vol: 1.0 }]);
    };

    media.playCompAudioLinear = function() {
      media.stopAudio();
      media.playAudio([{ src: "/android_asset/www/res/wavs/CompAudioTestLinear.wav", vol: 1.0 }]);
    };

    media.play1kHz94dB = function() {
      media.stopAudio();
      var wavfile = {
        path: "/android_asset/www/res/wavs/1kHz_cal_tone.wav",
        playbackMethod: "arbitrary",
        targetSPL: 94
      };

      // A seperate calibration is needed for each distinct tablet/headset
      // combination. Each calibration must have:
      //      {'wavRMSZ': 0.70231, 'RMSZ': 0.70231, 'scaleFactor': X.XXXXX};
      // The RMSs always have these values - a function of the wav file.
      // The scaleFactor is hardware dependent and can be found in the
      // tablet_headset-audio_profile.json file.
      if (disk.headset === "VicFirth") {
        wavfile.cal = { wavRMSZ: 0.70231, RMSZ: 0.70231, scaleFactor: 0.60027 };
      } else if (disk.headset === "VicFirthS2") {
        wavfile.cal = {
          wavRMSZ: 0.70231,
          RMSZ: 0.70231,
          scaleFactor: 0.2330724734026138
        };
      } else if (disk.headset === "HDA200") {
        wavfile.cal = { wavRMSZ: 0.70231, RMSZ: 0.70231, scaleFactor: 0.2774 };
      } else if (disk.headset === "Creare Headset" || disk.headset === "WAHTS") {
        wavfile.cal = {
          wavRMSZ: 0.70231,
          RMSZ: 0.70231,
          scaleFactor: 0.13519823071552697
        };
      } else if (disk.headset === "EPHD1") {
        wavfile.cal = {
          wavRMSZ: 0.70231,
          RMSZ: 0.70231,
          scaleFactor: 0.09404459105486002
        };
      } else if (disk.headset === "Audiometer") {
        wavfile.cal = { wavRMSZ: 1, RMSZ: 1, scaleFactor: 1 };
      } else {
        // default to VicFirth
        notifications.alert(gettextCatalog.getString("No calibrated sound available for this headset."));
        return;
      }

      media.playWav([wavfile], 0);
    };

    return media;
  });
