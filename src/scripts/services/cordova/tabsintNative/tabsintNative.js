/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

/*global TabSINTNative */

"use strict";

import * as angular from "angular";

angular
  .module("tabsint.services.cordova.tabsintNative", [])

  .factory("tabsintNative", function(chaStreaming, cordova, devices, disk, gettextCatalog, logger, notifications) {
    /* Wrapper for TabSINTNative to filter ios calls

       Newer cordova versions seem to grab the plugin despite the plugin.xml
       lacking a platform=ios section */

    var tabsintNative = {
      initialize: undefined,
      resetAudio: undefined,
      setAudio: undefined,
      getAudioVolume: undefined,
      onVolumeError: undefined,
      usbDeviceEventHandler: undefined,
      usbDeviceErrorHandler: undefined,
      registerUsbDeviceListener: undefined,
      unregisterUsbDeviceListener: undefined,
      isUsbConnected: undefined,
      usbEventListeners: [],
      cachedVolumePercent: undefined,
      scanFlicButton: undefined,
      disconnectFlicButton: undefined,
      getConnectedFlicButtons: undefined,
      buffer: undefined
    };

    tabsintNative.initialize = function(successCallback, errorCallback) {
      cordova.ready().then(function() {
        if (devices.platform.toLowerCase() === "android") {
          TabSINTNative.initialize(function() {
            logger.debug("tabsintNative initialized, getting volume to cache.");
            TabSINTNative.getAudioVolume(function(msg) {
              tabsintNative.cachedVolumePercent = msg.volume;
              logger.debug("cachedVolume = " + tabsintNative.cachedVolumePercent);
              if (successCallback && typeof successCallback === "function") {
                successCallback();
              }
            }, errorCallback);
            TabSINTNative.registerUsbDeviceListener(
              tabsintNative.usbDeviceEventHandler,
              tabsintNative.usbDeviceErrorHandler
            );
          }, errorCallback);
        } else if (devices.platform.toLowerCase() === "ios") {
          // iOS - do nothing
        } else if (devices.platform.toLowerCase() === "browser") {
          // browser - do nothing
        } else {
          notifications.alert(gettextCatalog.getString("ERROR: TabSINT Native Failed to Initialize"));
          logger.error("TabSINT Native failed to initialize. devices.platform: " + devices.platform);
        }
      });
    };

    tabsintNative.scanFlicButton = function(successCallback, errorCallback) {
      cordova.ready().then(function() {
        if (devices.platform.toLowerCase() === "android") {
          TabSINTNative.scanFlicButton(successCallback, errorCallback);
        }
      });
    };

    tabsintNative.disconnectFlicButton = function(successCallback, errorCallback, button) {
      cordova.ready().then(function() {
        if (devices.platform.toLowerCase() === "android") {
          TabSINTNative.disconnectFlicButton(successCallback, errorCallback, button);
        }
      });
    };

    tabsintNative.getConnectedFlicButtons = function(successCallback, errorCallback) {
      cordova.ready().then(function() {
        if (devices.platform.toLowerCase() === "android") {
          TabSINTNative.getConnectedFlicButtons(successCallback, errorCallback);
        }
      });
    };

    tabsintNative.registerUsbDeviceListener = function(callback) {
      cordova.ready().then(function() {
        if (devices.platform.toLowerCase() === "android") {
          tabsintNative.usbEventListeners.push(callback);
        }
      });
    };

    tabsintNative.unregisterUsbDeviceListener = function(callback) {
      cordova.ready().then(function() {
        if (devices.platform.toLowerCase() === "android") {
          if (tabsintNative.usbEventListeners.length > 0) {
            tabsintNative.usbEventListeners = tabsintNative.usbEventListeners.filter(function(value, index, arr) {
              return arr[index] !== callback;
            });
          }
        }
      });
    };

    tabsintNative.usbDeviceEventHandler = function(result) {
      if (angular.isDefined(tabsintNative.buffer)) {
        clearTimeout(tabsintNative.buffer);
      }

      function changeUsbDeviceStatus() {
        tabsintNative.isUsbConnected = result.isUsbConnected;
        var index;
        for (index = 0; index < tabsintNative.usbEventListeners.length; index++) {
          var callback = tabsintNative.usbEventListeners[index];
          if (callback && typeof callback === "function") {
            callback(tabsintNative.isUsbConnected);
          }
        }
      }

      logger.debug("tabsintNative usbDeviceEventHandler fired. Result: " + JSON.stringify(result));
      if (result && result.hasOwnProperty("isUsbConnected")) {
        if (result.isUsbConnected === false) {
          tabsintNative.buffer = setTimeout(changeUsbDeviceStatus, 3000);
        } else {
          changeUsbDeviceStatus();
        }
      } // TODO: Should the else set to false?
    };

    tabsintNative.usbDeviceErrorHandler = function(message) {
      logger.debug("tabsintNative.usbDeviceErrorHandler fired. Error: " + JSON.stringify(message));
      tabsintNative.isUsbConnected = false; // When errors occur, assume no connection.
    };

    tabsintNative.resetAudio = function(successCallback, errorCallback) {
      cordova.ready().then(function() {
        if (devices.platform.toLowerCase() === "android") {
          // override the resetAudio command is we have disabled automatic volume control
          // override when we are streaming to avoid a large beep (#442)
          if (disk.disableVolume || chaStreaming.streaming) {
            if (successCallback && typeof successCallback === "function") {
              successCallback();
            } // call success callback
          } else {
            logger.debug("Reset audio.");
            TabSINTNative.resetAudio(successCallback, errorCallback);
          }
        } else if (devices.platform.toLowerCase() === "ios") {
          // iOS - do nothing
        } else if (devices.platform.toLowerCase() === "browser") {
          // browser - do nothing
        } else {
          notifications.alert(gettextCatalog.getString("ERROR: TabSINT Native Failed to Reset Audio"));
          logger.error("TabSINT Native failed to Reset Audio. devices.platform: " + devices.platform);
        }
      });
    };

    tabsintNative.setAudio = function(successCallback, errorCallback, volume) {
      cordova.ready().then(function() {
        if (devices.platform.toLowerCase() === "android") {
          // override the resetAudio command is we have disabled automatic volume control
          if (disk.disableVolume) {
            if (successCallback && typeof successCallback === "function") {
              successCallback();
            } // call success callback
          } else {
            if (!volume) {
              volume = tabsintNative.cachedVolumePercent;
            }
            logger.debug("Set audio to " + volume + ".");
            TabSINTNative.setAudio(successCallback, errorCallback, volume);
          }
        } else if (devices.platform.toLowerCase() === "ios") {
          // iOS - do nothing
        } else if (devices.platform.toLowerCase() === "browser") {
          // browser - do nothing
        } else {
          notifications.alert(gettextCatalog.getString("ERROR: TabSINT Native Failed to Set Audio"));
          logger.error("TabSINT Native failed to Set Audio. devices.platform: " + devices.platform);
        }
      });
    };

    tabsintNative.getAudioVolume = function(successCallback, errorCallback) {
      cordova.ready().then(function() {
        if (devices.platform.toLowerCase() === "android") {
          TabSINTNative.getAudioVolume(successCallback, errorCallback);
        } else if (devices.platform.toLowerCase() === "ios") {
          // iOS - do nothing
        } else if (devices.platform.toLowerCase() === "browser") {
          // browser - do nothing
        } else {
          notifications.alert(gettextCatalog.getString("ERROR: TabSINT Native Failed to Get Audio Volume"));
          logger.error("TabSINT Native failed to getAudioVolume. devices.platform: " + devices.platform);
        }
      });
    };

    tabsintNative.onVolumeError = function(e) {
      //for internal use
      if (e.code === 99) {
        logger.error("Failed to set volume while running tabsintNative.resetAudio() with error: " + angular.toJson(e));
        logger.info("Volume error. Volume set to: " + e.volume);
        var msg =
          gettextCatalog.getString(
            "TabSINT was unable to set the volume to 100%. This is required for playing calibrated audio through the tablet."
          ) +
          "\n\n" +
          gettextCatalog.getString("Please set the volume to 100% manually to avoid seeing this error again.") +
          "\n\n" +
          gettextCatalog.getString("See the documentation for more information.");
        notifications.alert(msg);
      }
    };

    return tabsintNative;
  });
