/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";
import * as _ from "lodash";

angular
  .module("tabsint.components.flic.admin", [])

  .directive("flicAdmin", function() {
    return {
      restrict: "E",
      templateUrl: "scripts/components/flic-admin/flic-admin.html",
      controller: "flicAdminCtrl",
      scope: {}
    };
  })
  .controller("flicAdminCtrl", function(
    $scope,
    bluetoothStatus,
    tabsintNative,
    logger,
    notifications,
    gettextCatalog,
    flic
  ) {
    $scope.bluetoothStatus = bluetoothStatus;
    $scope.flic = flic;
    tabsintNative.getConnectedFlicButtons(success => {
      logger.info("success.buttons = " + success.buttons);
      flic.buttons = [];
      success.buttons.forEach((item, index) => {
        flic.buttons.push({ name: item });
      });
    });

    /**
     * Sets the class for the button entry in the table
     */
    $scope.pclass = function(p) {
      if (_.isEqual($scope.selected, p)) {
        return "table-selected";
      } else {
        return "";
      }
    };

    $scope.select = function(button) {
      $scope.selected = button;
    };

    /**
     * Disconnect from the selected button
     */
    $scope.disconnect = function() {
      if (!$scope.selected) {
        return;
      }

      notifications.confirm(gettextCatalog.getString("Disconnect from Flic button ") + $scope.selected.name, function(
        buttonIndex
      ) {
        if (buttonIndex === 1) {
          tabsintNative.disconnectFlicButton(
            success => {
              logger.info(success);
            },
            error => {
              logger.error(error);
            },
            $scope.selected.name
          );
          var index = _.findIndex(flic.buttons, $scope.selected);
          if (index === -1) {
            logger.error("Trying to disconnect from flic button " + $scope.selected.name + ", but it does not exist");
            return;
          }
          flic.buttons.splice(index, 1);
          $scope.selected = undefined;
        }
      });
    };
  });
