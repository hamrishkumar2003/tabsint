/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";

angular
  .module("tabsint.components.response-areas.seesaw", [])

  .controller("SeeSawResponseAreaCtrl", function($scope, examLogic, page) {
    $scope.$watch("page.result.response", function() {
      $scope.page.dm.isSubmittable = examLogic.getSubmittableLogic(page.dm.responseArea);
    });

    page.result.response = "";

    $scope.chosen = function(side) {
      if (side === "L") {
        return $scope.i === page.result.response;
      } else if (side === "R") {
        return 4 - $scope.i === page.result.response;
      }
    };

    $scope.btnSrc = function(side) {
      var btnSrc;
      if ($scope.chosen(side)) {
        btnSrc = "img/radioSeeSaw_selected_24_24.png";
      } else {
        btnSrc = "img/radioSeeSaw_unselected_24_24.png";
      }
      return btnSrc;
    };

    $scope.choose = function(side) {
      if (side === "L") {
        page.result.response = $scope.i;
      } else if (side === "R") {
        page.result.response = 4 - $scope.i;
      }
    };
  });
