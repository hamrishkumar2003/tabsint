/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";
import * as $ from "jquery";

/* jshint bitwise: false */
/* globals ChaWrap: false */

angular
  .module("tabsint.components.response-areas.cha.threshold-check", [])
  .directive("thresholdCheckExam", function() {
    return {
      restrict: "E",
      templateUrl: "scripts/components/response-areas/threshold-check/threshold-check.html",
      controller: "thresholdCheckExamCtrl"
    };
  })
  .controller("thresholdCheckExamCtrl", function(
    $q,
    $sce,
    $scope,
    $timeout,
    $window,
    cha,
    chaExams,
    examLogic,
    file,
    gettextCatalog,
    logger,
    page,
    results
  ) {
    /**********************************************************
     *  Setup - these steps happen as soon as the ctrl loads  *
     *********************************************************/
    var btnClassRight = ["btn btn-cha-right", "btn btn-cha-right active"];
    var btnClassLeft = ["btn btn-cha-left", "btn btn-cha-left active"];
    $scope.btnClassRight = btnClassRight[0];
    $scope.btnClassLeft = btnClassLeft[0];

    $scope.state = undefined;
    $scope.btnTextLeft = gettextCatalog.getString("LEFT");
    $scope.btnTextRight = gettextCatalog.getString("RIGHT");

    page.dm.instructionText = ""; // Override instruction text while testing to give more room for the button
    page.dm.isSubmittable = false;

    // create a new exam
    newThresholdResponseTimeExam();

    /**********************************************************
     *  Select a button - set software button state and push response 'left' or 'right'  *
     *********************************************************/
    $scope.select = function(earSide) {
      cha.setSoftwareButtonState(1);
      if (earSide === "right") {
        $scope.btnClassRight = btnClassRight[1];
      } else {
        $scope.btnClassLeft = btnClassLeft[1];
      }

      // need to toggle the software button class in case button is swiped
      // that is the way to make button appear "clicked" if it is swiped
      $timeout(function() {
        if (earSide === "right") {
          $scope.btnClassRight = btnClassRight[0];
        } else {
          $scope.btnClassLeft = btnClassLeft[0];
        }
      }, 200);

      // need to toggle the software button back to 0
      // put on timeout at 0 so it moves to the end of the callstack
      $timeout(function() {
        if ($scope.state === "running") {
          cha.setSoftwareButtonState(0);
        }
      }, 0);

      // Push response to result object
      page.result.response.push(earSide);
    };

    $scope.pressSoftwareButtonEnd = function() {};

    /**********************************************************
     *  Setup - get results from the CHA, process and submit results  *
     *********************************************************/
    function newThresholdResponseTimeExam() {
      $scope.state = "running";
      page.result = $.extend({}, results.default(page.dm), {
        examType: chaExams.examType,
        examProperties: chaExams.examProperties,
        response: [],
        correct: []
      });

      return chaExams.wait
        .forReadyState()
        .then(cha.requestResults)
        .then(function(results) {
          // load result onto page
          page.result = $.extend({}, page.result, results);

          // save current chaInfo (after test) to results
          page.result.chaInfo = chaExams.getChaInfo();

          // translate ActualEars into 0, 1
          // TODO: should ActualEars be booleans? this will work either way (0 interpreted as false)
          page.result.ActualEars = page.result.ActualEars.map(function(ear) {
            return ear ? 1 : 0;
          });

          // grade responses and populate page.result.correct array
          var mapEars = {
            0: "left",
            1: "right"
          };
          page.result.correct = page.result.ResponseTime.map(function(responseTime, idx) {
            if (responseTime > 0) {
              return page.result.response[idx] === mapEars[page.result.ActualEars[idx]];
            } else {
              page.result.response.splice(idx, 0, null); // insert null in responses where no response inserted
              return false;
            }
          });

          // make page submittable (it should already be because of responses)
          page.dm.isSubmittable = true;

          // result prepared, now ready to submit.
          // Auto-submit page if autoSubmit is enabled
          if (page.dm.responseArea.autoSubmit) {
            // submit after a short delay
            examLogic.submit();
          } else {
            $scope.state = "done";
            //update instructions to instruct user to submit if not auto-submitting
            $scope.thresholdCheckInstructions = "Complete, press Submit to continue.";
          }
          return $q.promise;
        })
        .catch(function(err) {
          cha.errorHandler.main(err);
        });
    }
  });
