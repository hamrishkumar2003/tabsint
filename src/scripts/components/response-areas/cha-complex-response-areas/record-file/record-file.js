/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";
import * as _ from "lodash";
import * as $ from "jquery";

/* jshint bitwise: false */
/* globals ChaWrap: false */

angular
  .module("cha.record-file", [])
  .directive("recordFile", function() {
    return {
      restrict: "E",
      templateUrl: "scripts/components/response-areas/cha-complex-response-areas/record-file/record-file.html",
      controller: "RecordFileCtrl"
    };
  })
  .controller("RecordFileCtrl", function(
    $q,
    $scope,
    $timeout,
    cha,
    chaExams,
    chaResults,
    chaConstants,
    disk,
    examLogic,
    gettextCatalog,
    logger,
    notifications,
    page,
    results
  ) {
    //define variables
    var examType = page.dm.audiometryType || "RecordFile"; // remove 'cha' from prefix
    // var examProperties = {
    //   EnableSynchronizationFile: 1
    // };
    // TODO: examProperties should be grabbed from the protocol eventually
    var examProperties = {};
    var resultsTimer = 250;
    page.dm.isSubmittable = true;
    $scope.btnState = 0;
    //this recordFiles is for testing
    // $scope.recordFiles = {
    //   0: ["startTime0", "endTime0", "filename0", "0"],
    //   1: ["startTime1", "endTime1", "filename1", "100"],
    //   2: ["startTime2", "In Progress", "filename2", "200"]
    // };
    $scope.recordFiles = {};
    $scope.currentRecordFile = 0;
    // $scope.currentRecordFile = 2;

    examLogic.submit = function() {
      submitResults();
    };

    function submitResults() {
      console.log("Results submitted");
      examLogic.submit = examLogic.submitDefault;
      examLogic.submit();
    }

    $scope.startRecording = function() {
      if ($scope.btnState === 1) {
        console.log("Stopping recording...");
        $scope.btnState = 0;
        stopRecording();
      } else {
        console.log("Starting recording...");
        $scope.btnState = 1;
        record();
      }
    };

    function stopRecording() {
      //update Record File metadata
      $scope.recordFiles[$scope.currentRecordFile][1] = new Date().toISOString();
      $scope.currentRecordFile += 1;
      return cha.abortExams().then(function() {
        console.log("Exam has been aborted");
        $scope.btnState = 0;
      });
    }

    function record() {
      cha
        .requestStatusBeforeExam()
        .then(res => {
          console.log("res", res);
          //need to queue exam with current timestamp
          /* seems to take bits (shifted)  - see https://cha.crearecomputing.net/cha-docs/CHA/protocols/record%20file.html
        DateTime to use when writing files. Optional.
          Coded in FAT date/time format as:
          bit31:25 : Year origin from 1980 (0..127)
          bit24:21 : Month (1..12)
          bit20:16 : Day (1..31)
          bit15:11 : Hour (0..23)
          bit10:5 : Minute (0..59)
          bit4:0 : Second/2 (0:29)
        */
          var curDate = new Date();
          console.log("Queuing RecordFile exam @ time: " + curDate.toString());

          var FATTime =
            ((curDate.getFullYear() - 1980) << 25) |
            ((curDate.getMonth() + 1) << 21) |
            (curDate.getDate() << 16) |
            (curDate.getHours() << 11) |
            (curDate.getMinutes() << 5) |
            (curDate.getSeconds() >> 1);

          $scope.recordFiles[$scope.currentRecordFile] = [
            curDate.toISOString(),
            "In Progress",
            "loading...",
            "loading..."
          ];

          examProperties.DateTime = curDate.toISOString();
          // examProperties.TimeOut = 0;
          console.log("examProperties", examProperties);
          return cha.queueExam("RecordFile", examProperties);
        })
        .then(chaExams.wait.forReadyState2)
        .then(() => {
          console.log("getExamResults() will run shortly");
          return setTimeout(getExamResults, resultsTimer);
        })
        .catch(function(err) {
          console.log("CHA ERROR (in catch)");
          console.log(err);
          cha.errorHandler.main(err);
        });
    }

    function getExamResults() {
      console.log("inside getExamResults");
      if ($scope.btnState == 0) {
        console.log("Exam aborted, cancelling continuous results requests");
        return;
      } else {
        return cha
          .requestResults()
          .then(res => {
            console.log("res", res);
            //update metadata
            if (res) {
              try {
                $scope.recordFiles[$scope.currentRecordFile][2] = res.DirectoryName;
                $scope.recordFiles[$scope.currentRecordFile][3] = res.AudioSampleCount;
              } catch {
                console.log("Warning: Exam aborted after results were requested.");
              }
            } else {
              $scope.recordFiles[$scope.currentRecordFile][2] = "Error?";
              $scope.recordFiles[$scope.currentRecordFile][3] = "Error?";
            }
            //proceed with another examSubmission
            var jan_one_2000 = 946684800; //time in seconds from jan 1 2000 to unix time
            var ts = (new Date().getTime() / 1000 - jan_one_2000) * 1000000000; //time in ns since jan 1 2000 (uint64)
            var devId = 255;
            var params = {
              Timestamp: ts,
              DeviceIdx: devId
            };
            console.log("params", params);
            return cha.examSubmission("RecordFile$Submission", params);
          })
          .then(() => {
            console.log("getExamResults() will run shortly");
            return setTimeout(getExamResults, resultsTimer);
            // setTimeout(getExamResults, resultsTimer);
          })
          .catch(() => {
            console.log("Error in getExamResults (in catch)");
          });
      }
    }

    $scope.recordButtonClass = function() {
      let presentButtonClass = ["btn btn-block btn-record", "btn btn-block btn-record active"];
      return presentButtonClass[$scope.btnState];
    };
  });
