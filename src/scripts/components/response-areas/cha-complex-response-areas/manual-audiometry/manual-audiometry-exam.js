/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";
import * as _ from "lodash";
import * as $ from "jquery";

/* jshint bitwise: false */
/* globals ChaWrap: false */

angular
  .module("cha.manual-audiometry-exam", [])
  .directive("manualAudiometryExam", function() {
    return {
      restrict: "E",
      templateUrl:
        "scripts/components/response-areas/cha-complex-response-areas/manual-audiometry/manual-audiometry-exam.html",
      controller: "ManualAudiometryExamCtrl"
    };
  })
  .controller("ManualAudiometryExamCtrl", function(
    $q,
    $scope,
    $timeout,
    cha,
    chaExams,
    chaResults,
    chaConstants,
    disk,
    examLogic,
    gettextCatalog,
    logger,
    notifications,
    page,
    results
  ) {
    //Defining parameters
    var examType = page.dm.audiometryType || "ManualAudiometryExam";
    var presentationIndex = 0;
    //page.dm.id can increase and change? This is what the table and the plot expect to generate
    //I am currently ignoring this parameter and will produce plot a different way
    //the table seems to be produced via a different method as well
    var defaultPresentationId = page.dm.id;
    console.log("defaultPresentationId", defaultPresentationId);
    var startTime = new Date();
    $scope.disablePlayTone = true;

    const defaultExamProperties = {
      StepSize: 5,
      F: 1000,
      OutputChannel: "HPL0"
    };
    const examPropertiesForQueueing = {
      BoneConduction: false,
      Masking: false,
      TeleMode: false
    };

    $scope.panes = {
      level: true,
      progression: false,
      table: true
    };
    $scope.btnState = 0;
    // $scope.showPresentedTones = page.dm.responseArea.showPresentedTones;
    $scope.showPresentedTones = true;
    $scope.presentationList = angular.copy(page.dm.responseArea.presentationList);
    $scope.increaseValue = defaultExamProperties.StepSize;
    $scope.decreaseValue = defaultExamProperties.StepSize * 2;

    //queueExam
    cha
      .requestStatusBeforeExam()
      .then(function(res) {
        console.log("res from requestStatusBeforeExam", res);
        console.log("Exam has been queued");
        return cha.queueExam("ManualAudiometry", examPropertiesForQueueing);
      })
      .catch(function(err) {
        cha.errorHandler.main(err);
      });

    examLogic.submit = function() {
      submitResults();
    };
    page.dm.isSubmittable = true;

    // build presentation list
    if (
      angular.isUndefined(chaExams.storage.manualAudiometry) ||
      angular.isUndefined(chaExams.storage.manualAudiometry.list)
    ) {
      chaExams.storage.manualAudiometry = {
        list: {}
      };
      chaExams.storage.manualAudiometry.list.Left = angular.copy(page.dm.responseArea.presentationList);
      chaExams.storage.manualAudiometry.list.Right = angular.copy(page.dm.responseArea.presentationList);

      // build exams for left ear
      _.forEach(chaExams.storage.manualAudiometry.list.Left, function(pres) {
        var examProps = _.extend({}, defaultExamProperties, pres, {
          PresentationMax: 1
        });
        console.log("examProps", examProps);
        examProps.OutputChannel = "HPL0";
        pres.examProperties = examProps;
      });

      // build exames for right ear
      _.forEach(chaExams.storage.manualAudiometry.list.Right, function(pres) {
        var examProps = _.extend({}, defaultExamProperties, pres, {
          PresentationMax: 1
        });
        examProps.OutputChannel = "HPR0";
        pres.examProperties = examProps;
      });
    }

    // get width of plot container
    let containerWidth = document.documentElement.clientWidth;
    $scope.plotContainerWidth = 0.8 * containerWidth;
    $scope.plotContainerWidth = $scope.plotContainerWidth > 300 ? $scope.plotContainerWidth : 300;

    // get height of device view
    let documentHeight = document.documentElement.clientHeight;
    $scope.plotContainerHeight = Math.round(documentHeight / 2.5);
    $scope.plotContainerHeight = $scope.plotContainerHeight > 300 ? $scope.plotContainerHeight : 300;

    // view fields
    $scope.data = {
      currentFrequency: undefined,
      currentLevel: undefined,
      channel: undefined, // default set below based on protocol inputs
      levelUnits: undefined,
      minLevel: undefined,
      maxLevel: undefined
    };

    // Determine if it's a threshold or pass-fail exam
    $scope.data.responseType = "threshold";

    // set min and max levels, handling undefined and 0 cases
    if (angular.isDefined(page.dm.responseArea.minLevel)) {
      $scope.data.minLevel = page.dm.responseArea.minLevel;
    } else {
      $scope.data.minLevel = -10;
    }
    if (angular.isDefined(page.dm.responseArea.maxLevel)) {
      $scope.data.maxLevel = page.dm.responseArea.maxLevel;
    } else {
      $scope.data.maxLevel = 90;
    }

    // Starting channel.  Start with channel defined in exame props, or default left
    var channel = defaultExamProperties.OutputChannel || "HPL0";
    $scope.btnText = ["Play Tones", "Stop Tones"];
    if (channel.indexOf("HPR") > -1) {
      $scope.data.channel = "Right";
      $scope.translatableChannel = gettextCatalog.getString($scope.data.channel);
    } else if (channel.indexOf("HPL") > -1) {
      $scope.data.channel = "Left";
      $scope.translatableChannel = gettextCatalog.getString($scope.data.channel);
    }

    // set level units
    var levelUnits =
      defaultExamProperties.LevelUnits ||
      chaExams.storage.manualAudiometry.list[$scope.data.channel][presentationIndex].examProperties.LevelUnits;
    if (angular.isUndefined(levelUnits)) {
      logger.warn("no levelUnits defined for chaManualAudiometry, defaulting to dB SPL");
      $scope.data.levelUnits = "dB SPL";
    } else {
      $scope.data.levelUnits = levelUnits;
    }

    //force dB SPL units
    $scope.data.levelUnits = "dB SPL";

    // start off in the first frequency
    $scope.tableNames = Object.keys(chaExams.storage.manualAudiometry.list);

    //Defining functions

    $scope.playTonesButtonClass = function() {
      let presentButtonClass;
      if ($scope.data.channel === "Right") {
        presentButtonClass = ["btn btn-block btn-danger", "btn btn-block btn-danger active"];
      } else if ($scope.data.channel === "Left") {
        presentButtonClass = ["btn btn-block btn-primary", "btn btn-block btn-primary active"];
      } else {
        presentButtonClass = ["btn btn-block btn-default", "btn btn-block btn-default active"];
      }
      return presentButtonClass[$scope.btnState];
    };

    //Check that data.currentLevel falls within the data.minLevel and
    //data.maxLevel bounds - this isn't a thorough check, as the headset may
    //be calibrated with tighter bounds at particular frequencies
    function levelBoundsCheck() {
      if ($scope.data.currentLevel >= $scope.data.maxLevel) {
        $scope.data.currentLevel = $scope.data.maxLevel;
      } else if ($scope.data.currentLevel <= $scope.data.minLevel) {
        $scope.data.currentLevel = $scope.data.minLevel;
      }
      setTimeout(() => {
        console.log("playToneEnabled");
        $scope.disablePlayTone = false;
        $scope.$apply();
      }, 500);
    }

    $scope.increase = function(val) {
      $scope.disablePlayTone = true;
      // stopPresentation(); // stop presentation, if started
      $scope.data.currentLevel += val;
      levelBoundsCheck();
    };

    $scope.decrease = function(val) {
      $scope.disablePlayTone = true;
      // stopPresentation(); // stop presentation, if started
      $scope.data.currentLevel -= val;
      levelBoundsCheck();
    };

    function setFrequencyAndLevel() {
      $scope.disablePlayTone = true;
      $scope.data.currentFrequency = chaExams.storage.manualAudiometry.list[$scope.data.channel][presentationIndex].F;
      // switching to a new frequency.  If we already used this frequency, then there should be a threshold
      if (
        $scope.data.responseType === "threshold" &&
        angular.isDefined(chaExams.storage.manualAudiometry.list[$scope.data.channel][presentationIndex].Threshold)
      ) {
        $scope.data.currentLevel =
          chaExams.storage.manualAudiometry.list[$scope.data.channel][presentationIndex].Threshold;
      } else if (
        angular.isDefined(
          chaExams.storage.manualAudiometry.list[$scope.data.channel][presentationIndex].L &&
            chaExams.storage.manualAudiometry.list[$scope.data.channel][presentationIndex].L.length > 0
        )
      ) {
        $scope.data.currentLevel = chaExams.storage.manualAudiometry.list[$scope.data.channel][
          presentationIndex
        ].L.slice(-1)[0];
        console.log("setFrequencyAndLevel audiometry list", chaExams.storage.manualAudiometry.list);
      } else {
        // no previous use, look for Lstart, otherwise default to 40
        $scope.data.currentLevel =
          chaExams.storage.manualAudiometry.list[$scope.data.channel][presentationIndex].Lstart ||
          defaultExamProperties.Lstart ||
          40; // default is 40
      }
      levelBoundsCheck();
    }

    $scope.chooseFrequency = function(index) {
      $scope.disablePlayTone = true;
      presentationIndex = index;
      setFrequencyAndLevel();
    };
    $scope.chooseFrequency(presentationIndex);

    function nextFrequency() {
      $scope.disablePlayTone = true;
      if (presentationIndex < chaExams.storage.manualAudiometry.list[$scope.data.channel].length - 1) {
        $scope.chooseFrequency(presentationIndex + 1);
      }

      // reset index to 0 and go to the next ear
      else {
        presentationIndex = 0;
        let channel_keys = Object.keys(chaExams.storage.manualAudiometry.list);
        let channel_idx = channel_keys.indexOf($scope.data.channel);
        let next_channel_idx = channel_idx === channel_keys.length - 1 ? 0 : channel_idx + 1;
        $scope.selectOutput(channel_keys[next_channel_idx]);
      }
      levelBoundsCheck();
    }

    $scope.setThreshold = function() {
      $scope.disablePlayTone = true;
      console.log("$scope.data.channel", $scope.data.channel);
      console.log("$scope.data.currentLevel", $scope.data.currentLevel);
      console.log("$scope.data.currentFrequency", $scope.data.currentFrequency);

      // store current level
      chaExams.storage.manualAudiometry.list[$scope.data.channel][presentationIndex].Threshold = Math.round(
        $scope.data.currentLevel
      );
      // move on to the next frequency
      nextFrequency();
    };

    $scope.selectOutput = function(channel) {
      $scope.disablePlayTone = true;
      console.log("channel", channel);
      // return immediately if channel is not actually changed
      if ($scope.data.channel === channel) {
        levelBoundsCheck();
        return;
      }
      $scope.data.channel = channel; // set channel
      $scope.translatableChannel = gettextCatalog.getString($scope.data.channel);
      setFrequencyAndLevel();
      presentationIndex = 0; // reset presentation number
      levelBoundsCheck();
    };

    function playTone() {
      $scope.btnState = 1;
      if (!angular.isDefined(chaExams.storage.manualAudiometry.list[$scope.data.channel][presentationIndex].L)) {
        chaExams.storage.manualAudiometry.list[$scope.data.channel][presentationIndex].L = [];
      }
      chaExams.storage.manualAudiometry.list[$scope.data.channel][presentationIndex].L.push($scope.data.currentLevel);
      chaExams.storage.manualAudiometry.list[$scope.data.channel][presentationIndex].examProperties.Lstart =
        $scope.data.currentLevel;
      var examProperties =
        chaExams.storage.manualAudiometry.list[$scope.data.channel][presentationIndex].examProperties;
      var recordedStartTime = false;
      var pres = chaExams.storage.manualAudiometry.list[$scope.data.channel][presentationIndex];
      var presId = defaultPresentationId + "_" + $scope.data.channel + "_HW_" + pres.examProperties.F;
      console.log("presId", presId);

      if (page.dm.responseArea.onlySubmitFrequenciesTested) {
        if (angular.isUndefined(pres.Threshold)) {
          return;
        }
      }

      page.result = results.default(page.dm);
      if (!recordedStartTime) {
        page.result.responseStartTime = startTime;
        recordedStartTime = true;
      }

      page.result = $.extend({}, page.result, {
        examType: examType,
        presentationId: presId,
        chaInfo: chaExams.getChaInfo(),
        ResponseType: "Threshold",
        presentationIndex: 0,
        ResultType: angular.isDefined(pres.Threshold) ? "Threshold" : "Skipped",
        Threshold: angular.isDefined(pres.Threshold) ? pres.Threshold : Number.NaN,
        RetSPL: 0,
        Units: "dB SPL",
        L: pres.L,
        response: angular.isDefined(pres.Threshold) ? pres.Threshold : Number.NaN,
        examProperties: pres.examProperties
      });

      //this contains all the storage information about tones that have been played
      console.log("chaExams", chaExams);
      console.log("examProperties", examProperties);
      console.log("$scope.data.channel", $scope.data.channel);
      console.log("$scope.data.currentLevel", $scope.data.currentLevel);
      console.log("$scope.data.currentFrequency", $scope.data.currentFrequency);

      return (
        // cha
        //   .examSubmission("ManualAudiometry$Submission", {
        //     F: examProperties.F,
        //     TestEar: examProperties.TestEar,
        //     Lstart: examProperties.Lstart,
        //     PlayStimulus: examProperties.PlayStimulus
        //   })
        cha
          .examSubmission("ManualAudiometry$Submission", {
            F: $scope.data.currentFrequency,
            OutputChannel: $scope.data.channel == "Left" ? "HPL0" : "HPR0",
            Lstart: $scope.data.currentLevel,
            PlayStimulus: true
          })
          //sleep for a second
          .then(() => new Promise(resolve => setTimeout(resolve, 1000)))
          .then(buttonHandler)
          .then(getPresentationInfo)
          .then(resetResults)
          .catch(function(err) {
            cha.errorHandler.main(err);
            //see if queueExam works
            console.log("err", err);
          })
      );
    }

    function buttonHandler() {
      var deferred = $q.defer();
      if ($scope.btnState === 1) {
        $scope.btnState = 0;
      }
      deferred.resolve();
      return deferred.promise;
    }

    function resetResults() {
      var deferred = $q.defer();
      console.log("resetting results, this needs to be changed");
      var presentationId = page.result.presentationId;
      page.result = {};
      page.result = {
        examType: chaExams.examType,
        examProperties: chaExams.examProperties,
        presentationId: presentationId,
        responseStartTime: new Date()
      };
      deferred.resolve(page);
      console.log("finished resetting the results");
      return deferred.promise;
    }

    function getPresentationInfo() {
      return cha
        .requestResults()
        .then(function(result) {
          //eventually need function here to handle process result
          console.log("result (getPresentationInfo)", result);
        })
        .catch(function(err) {
          cha.errorHandler.main(err);
        });
    }

    $scope.toggleToneButton = function() {
      console.log("About to play the tone");
      playTone();
    };

    function submitResults() {
      var earSide = "";
      var recordedStartTime = false;

      function submitSingleResult(pres, index) {
        // var presId = earSide + "_HW_" + pres.examProperties.F;
        var presId = defaultPresentationId + "_" + earSide + "_HW_" + pres.examProperties.F;
        console.log("presId", presId);

        if (page.dm.responseArea.onlySubmitFrequenciesTested) {
          if (angular.isUndefined(pres.Threshold)) {
            return;
          }
        }

        page.result = results.default(page.dm);
        if (!recordedStartTime) {
          page.result.responseStartTime = startTime;
          recordedStartTime = true;
        }
        page.result = $.extend({}, page.result, {
          examType: examType,
          presentationId: presId,
          chaInfo: chaExams.getChaInfo(),
          ResponseType: "Threshold",
          presentationIndex: index,
          ResultType: angular.isDefined(pres.Threshold) ? "Threshold" : "Skipped",
          Threshold: angular.isDefined(pres.Threshold) ? pres.Threshold : Number.NaN,
          RetSPL: 0,
          Units: "dB SPL",
          L: pres.L,
          response: angular.isDefined(pres.Threshold) ? pres.Threshold : Number.NaN,
          examProperties: pres.examProperties
        });
        logger.debug("CHA pushing maunal audiometry presentation result onto stack: " + angular.toJson(page.result));
        examLogic.pushResults();
      }

      earSide = "Left";
      _.forEach(chaExams.storage.manualAudiometry.list.Left, submitSingleResult);
      earSide = "Right";
      _.forEach(chaExams.storage.manualAudiometry.list.Right, submitSingleResult);

      page.result = {
        presentationId: "ManualAudiometryExam",
        responseStartTime: startTime,
        response: "complete"
      };

      // clear storage
      delete chaExams.storage.manualAudiometry;

      // push results
      examLogic.submit = examLogic.submitDefault;
      examLogic.submit();
    }

    cha.errorHandler.responseArea = function(err) {
      // Display error to user with popup notifications
      logger.error("CHA - manual-audiometry-exam error: " + JSON.stringify(err));
      $scope.chaError = true;

      if (err.msg) {
        //special error handling for exceeding maximum calibrated level.
        if (err.code === 10) {
          notifications.alert(
            "The target amplitude at this frequency is outside the calibrated bounds for this headset."
          );
          //reset button state
          $scope.btnState = 0;
          //reset the last entry on the list. No need to store something out of range.
          chaExams.storage.manualAudiometry.list[$scope.data.channel][presentationIndex].L.pop();
        } else if (err.code !== 1) {
          //ignore code 1, which is an error from trying to ask the WAHTS to do something while it's busy
          notifications.alert(err.msg);
        }
      } else {
        notifications.alert("Internal error, please restart the Manual Audiometry test.");
      }
    };
  });
