/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";

angular
  .module("tabsint.components.response-areas.textbox", [])

  .controller("TextboxResponseAreaCtrl", function($scope, examLogic, page) {
    $scope.page.result.response = page.result.response;

    $scope.$watch("page.result.response", function() {
      $scope.page.dm.isSubmittable = examLogic.getSubmittableLogic(page.dm.responseArea);
    });

    // Clear textbox when the question ID is changed
    function update() {
      $scope.page.result.response = undefined;

      if (!!page.dm.responseArea.rows) {
        $scope.rows = page.dm.responseArea.rows;
      } else {
        $scope.rows = 1;
      }

      if (page.dm.responseArea.submitEmpty) {
        $scope.page.result.response = "";
      }
    }

    update();
    //HG Directive bellow so page submits when pressing enter
    $scope.submitted = false; //is for debouncing, to prevent multiple submissions
  })
  .directive("enterClose", [
    "app",
    "cordova",
    function(app, cordova) {
      return function(scope, element, attr) {
        element.bind("keydown keypress", function(event) {
          if (event.which === 13 && event.key == "Enter") {
            //scope.page.dm.isSubmittable = false;
            //HG, &scope.page.dm.isSubmittable to prevent empty submits
            if (!scope.submitted & scope.page.dm.isSubmittable) {
              scope.submitted = true;
              scope.examLogic.submit();
            }
          }
        });
      };
    }
  ]);
