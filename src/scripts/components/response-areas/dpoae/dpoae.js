/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";
import * as $ from "jquery";

/* jshint bitwise: false */
/* globals ChaWrap: false */

angular
  .module("tabsint.components.response-areas.cha.dpoae", [])
  .directive("dpoaeExam", function() {
    return {
      restrict: "E",
      templateUrl: "scripts/components/response-areas/dpoae/dpoae.html",
      controller: "DPOAEExamCtrl"
    };
  })
  .controller("DPOAEExamCtrl", function($scope, $q, cha, page, disk, chaExams, chaResults, logger, examLogic) {
    function startExam() {
      // Expose the expected levels for F1 and F2
      console.log("chaExams", chaExams);
      $scope.targetF1Amplitude = chaExams.examProperties.L1;
      $scope.targetF2Amplitude = chaExams.examProperties.L2;
      console.log("L1", $scope.targetF1Amplitude);
      console.log("L2", $scope.targetF2Amplitude);

      // Attempting to find ear flag - needed for TOB as well
      console.log("examLogic", examLogic); // use the following: examLoigc.dm.flags.<flag_id>

      // Proceed as normal
      page.dm.isSubmittable = false;
      $scope.page.dm.questionMainText =
        "Running DPOAE exam for F1: " +
        Math.floor(chaExams.examProperties.F1) +
        " and F2: " +
        Math.floor(chaExams.examProperties.F2);
    }

    function handleResults(resultFromThisPage) {
      // Display information related to F1 and F2 Amplitudes from previous exam
      var maxAmplitudeDifference = 10;
      $scope.f1Amp = Math.round(resultFromThisPage.F1.Amplitude);
      $scope.f2Amp = Math.round(resultFromThisPage.F2.Amplitude);
      $scope.f1Style = { color: "green", "font-weight": "bold" };
      $scope.f2Style = { color: "green", "font-weight": "bold" };
      // Set color to red if outide of +/- maxAmplitudeDifference
      if (
        $scope.f1Amp < $scope.targetF1Amplitude - maxAmplitudeDifference ||
        $scope.f1Amp > $scope.targetF1Amplitude + maxAmplitudeDifference
      ) {
        $scope.f1Style = { color: "red", "font-weight": "bold" };
      }
      if (
        $scope.f2Amp < $scope.targetF2Amplitude - maxAmplitudeDifference ||
        $scope.f2Amp > $scope.targetF2Amplitude + maxAmplitudeDifference
      ) {
        $scope.f2Style = { color: "red", "font-weight": "bold" };
      }

      chaExams.state = "results"; // This is what enables the results view (plot)

      // Update page view, get rid of instructions
      page.dm.hideProgressbar = true;
      $scope.page.dm.title = "Results";
      $scope.page.dm.instructionText = "";

      // set examType and keep result
      page.result.examType = "dpoae";
      page.result.result = resultFromThisPage;

      // Add the ear to the results
      if (examLogic.dm.state.flags.startRight) {
        page.result.result.channel = "right";
      } else {
        page.result.result.channel = "left";
      }

      // Only plot if this page was given displayIds.
      if (angular.isDefined(page.dm.responseArea.displayIds)) {
        showPlot(page.result.result);
      }

      // enable autoSubmit
      if (page.dm.responseArea.autoSubmit) {
        examLogic.submit();
        return;
      } else {
        // set default 'response' field
        page.result.response = "continue";
        page.dm.isSubmittable = true;
      }
    }

    function showPlot(resultFromThisPage) {
      var data = {
        resultList: []
      };
      data.resultList.push(resultFromThisPage);

      // Get results for and display a plot of the dpoae data
      var results = chaResults.getPastResults(page.dm.responseArea.displayIds, null);
      $scope.dpoaeData = chaResults.createDPOAEData(results);
    }

    var waitFunction = function() {
      chaExams.wait
        .forReadyState()
        .then(function() {
          return cha.requestResults();
        })
        .then(handleResults)
        .catch(function(err) {
          cha.errorHandler.main(err);
        });
    };

    startExam();

    waitFunction();
  })

  .directive("dpoaePlot", function() {
    return {
      restrict: "E",
      template: '<div id="dpoaePlotWindow"></div>',
      controller: "DPOAEPlotCtrl",
      scope: {
        data: "="
      }
    };
  })

  .controller("DPOAEPlotCtrl", function($scope, d3Services) {
    d3Services.dpoaePlot("#dpoaePlotWindow", $scope.data);
  });
