/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";
import * as _ from "lodash";

angular
  .module("tabsint.components.response-areas.multiple-input", [])

  .controller("MultipleInputResponseAreaCtrl", function($scope, page) {
    $scope.list = page.dm.responseArea.inputList;
    var inputTypeForAll = page.dm.responseArea.inputTypeForAll;

    // optional display review/edit btns
    $scope.review = page.dm.responseArea.review;

    // get current date for date input properties, YYYY-MM-DD formatted string
    $scope.today = new Date().toISOString().slice(0, 10);

    // (dis/en)able review btns toggle
    $scope.enableReview = function(status) {
      if (status) $scope.reviewDisabled = true;
      else $scope.reviewDisabled = false;
    };

    // defaults
    $scope.multiDropdownModel = {};
    $scope.multiDropdownJson = {};
    var cnt = -1;
    $scope.list.forEach(function(item) {
      cnt += 1;
      if (item.inputType === angular.undefined) {
        item.inputType = inputTypeForAll || "text";
      }

      if (item.inputType === "multi-dropdown") {
        $scope.multiDropdownJson[cnt] = [];
        $scope.multiDropdownModel[cnt] = [];
        for (var i = 0; i < page.dm.responseArea.inputList[cnt].options.length; i++) {
          $scope.multiDropdownJson[cnt].push({
            id: i,
            label: page.dm.responseArea.inputList[cnt].options[i]
          });
        }
      }
    });

    // set response to values in the list
    $scope.page.result.response = _.map($scope.list, function(item) {
      return item.value;
    });

    // Dropdown logic
    $scope.selectResponse = function(itemIdx, option) {
      $scope.page.result.response[itemIdx] = option;
    };

    //Multi-dropdown logic
    $scope.selectMultiResponse = function(itemIdx) {
      var multiResp = [];
      $scope.multiDropdownModel[itemIdx].forEach(function(resp) {
        multiResp.push(resp["label"]);
      });
      $scope.page.result.response[itemIdx] = multiResp;
    };

    function getSubmittableLogic() {
      var res = true;
      $scope.list.forEach(function(item, idx) {
        if (item.required && isUndefined(item, $scope.page.result.response[idx])) {
          res = false;
        }
      });
      $scope.page.dm.isSubmittable = res;
    }
    getSubmittableLogic();

    // submission logic
    $scope.$watch(
      "page.result.response",
      function() {
        getSubmittableLogic();
      },
      true
    );

    // custom isUndefined function to handle text
    function isUndefined(item, val) {
      if (item.inputType === "text" || item.inputType === "number") {
        return val === "" || val === null || val === undefined;
      } else {
        return angular.isUndefined(val);
      }
    }
  });
