/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";
import _ from "angular-animate";
import { Set } from "es6-shim";

/* jshint bitwise: false */
/* globals ChaWrap: false */

angular
  .module("cha.audiometry-table", [])
  .controller("AudiometryTableCtrl", function($scope, page, chaResults, gettextCatalog, disk, protocol) {
    // Define functions
    $scope.replicateArray = function replicateArray(array, n) {
      var arrays = Array.apply(null, new Array(n));
      arrays = arrays.map(function() {
        return array;
      });
      return [].concat.apply([], arrays);
    };

    function parseScreenerResult(result) {
      if (result == "Pass" || result == "P") {
        return gettextCatalog.getString("Pass");
      } else if (result == "Refer" || result == "R") {
        return gettextCatalog.getString("REFER");
      } else if (result == "-" || result == "No Response") {
        return gettextCatalog.getString("-");
      } else if (result == "No Result") {
        return gettextCatalog.getString("-");
      } else {
        return result;
      }
    }

    function round(input) {
      if (typeof input === "number") {
        return Math.round(input);
      }
      return input;
    }

    // Logic
    page.dm.hideProgressbar = true;
    page.dm.isSubmittable = true;

    // Update page view, get rid of instructions
    $scope.showSLMNoise = page.dm.responseArea.showSLMNoise;
    $scope.showSvantek = page.dm.responseArea.showSvantek;
    $scope.page.dm.title = gettextCatalog.getString("Audiometry Results");
    $scope.page.dm.questionMainText = "";
    $scope.page.dm.instructionText = "";

    // this function caclulates audiometry results based on the display id's passed. It will return an array of [resultsList, audiogramData]
    // If we are generating a PDF, resultsList will already be defined as a subset of the results corresponding to a page of the protocol.
    // Otherwise, resultsList will be undefined (ex. showing results after test is complete)
    if (angular.isUndefined($scope.resultsList)) {
      $scope.resultsList = chaResults.createAudiometryResults(page.dm.responseArea.displayIds)[0];
    }

    // Check if there are results
    if (angular.isUndefined($scope.resultsList)) {
      $scope.page.dm.questionSubText = gettextCatalog.getString("No Results to Show");
    } else {
      // Remove duplicate ID's (if applicable) from the resultsList. This removes the oldest duplicates (keeps most recent data).
      var tmp = [];
      var tmpID = [];
      $scope.resultsList.forEach(e => {
        if (!tmpID.includes(e.presentationId)) {
          tmp.push(e);
          tmpID.push(e.presentationId);
        }
      });
      $scope.resultsList = tmp;
      // console.log("$scope.resultsList for audiometry-table", $scope.resultsList);

      // ExamType used for checking if it is BHAFT or masking
      $scope.examType = $scope.resultsList[0].examType;

      // If manual exam, remove svantek and slm
      if ($scope.resultsList[0].responseArea.toLowerCase().includes("manual")) {
        $scope.showSLMNoise = false;
        $scope.showSvantek = false;
      }

      // Check if exam had screening and do some pre-procesing
      if (
        $scope.resultsList[0].Screener ||
        $scope.resultsList[0].responseArea == "chaManualScreener" ||
        $scope.resultsList[0].examProperties.Screener
      ) {
        var thresh;
        $scope.screening = true;
        $scope.screens = {};
        if ($scope.resultsList[0].page.responseArea.levels) {
          $scope.screenerThresholds = $scope.resultsList[0].page.responseArea.levels;
        } else {
          $scope.screenerThresholds = [];
          $scope.resultsList.forEach(e => {
            if (!$scope.screenerThresholds.includes(e.examProperties.Lstart)) {
              $scope.screenerThresholds.push(e.examProperties.Lstart);
            }
          });
        }
        $scope.screenerThresholds.sort(function(a, b) {
          return a - b;
        });
      } else {
        $scope.screening = false;
      }

      // Check for masking and do some pre-processing
      if ($scope.examType == "MaskedThreshold" || $scope.resultsList[0].page.responseArea.masking) {
        $scope.LevelType = "Masked Threshold";
        $scope.masking = true;
        $scope.maskingLevels = {};
        $scope.maskingUnits = "dB EM";
      } else {
        $scope.LevelType = "Threshold";
        $scope.masking = false;
      }

      // Setting units
      if ($scope.resultsList[0].LevelUnits) {
        $scope.LevelUnits = $scope.resultsList[0].LevelUnits;
      } else if ($scope.resultsList[0].Units) {
        $scope.LevelUnits = $scope.resultsList[0].Units;
      } else if ($scope.resultsList[0].examProperties.LevelUnits) {
        $scope.LevelUnits = $scope.resultsList[0].examProperties.LevelUnits;
      }

      // Manual audiometry resultsList has entry even for skipped items
      // Automated audiometry resultsList only has entries for non-skipped items.
      // Need to add empty entries for missing items
      let channels = new Set();
      let frequencies = {};
      var leftCodes = ["HPL0", "HPL1", "LINEL0 NONE"];
      var rightCodes = ["HPR0", "HPR1", "NONE LINER0"];
      // Loop through results to find relevant channels and frequencies
      $scope.resultsList.forEach(element => {
        if (angular.isDefined(element.examProperties)) {
          // Channel
          var channel_adder;
          if (
            leftCodes.includes(element.examProperties.OutputChannel) &&
            !rightCodes.includes(element.examProperties.OutputChannel)
          ) {
            channel_adder = "Left";
          } else if (
            rightCodes.includes(element.examProperties.OutputChannel) &&
            !leftCodes.includes(element.examProperties.OutputChannel)
          ) {
            channel_adder = "Right";
          } else {
            channel_adder = "Binaural";
          }
          channels.add(channel_adder);
          // Frequency
          if (!frequencies[channel_adder]) {
            frequencies[channel_adder] = new Set();
          }
          if ($scope.examType != "BHAFT") {
            frequencies[channel_adder].add(element.examProperties.F / 1000);
          } else if ($scope.examType == "BHAFT") {
            frequencies[channel_adder].add(element.ThresholdFrequency ? round(element.ThresholdFrequency) : "-");
          }
        }
        // If no examProperties, this is an Automated Audiometry result that was skipped.
        else if (angular.isDefined(element.page)) {
          element.examProperties = {
            OutputChannel: element.page.responseArea.examProperties.OutputChannel,
            F: element.page.responseArea.examProperties.F,
            LevelUnits: element.page.responseArea.examProperties.LevelUnits
          };
          // Channel
          var channel_adder;
          if (
            leftCodes.includes(element.page.responseArea.examProperties.OutputChannel) &&
            !rightCodes.includes(element.page.responseArea.examProperties.OutputChannel)
          ) {
            channel_adder = "Left";
          } else if (
            rightCodes.includes(element.page.responseArea.examProperties.OutputChannel) &&
            !leftCodes.includes(element.page.responseArea.examProperties.OutputChannel)
          ) {
            channel_adder = "Right";
          } else {
            channel_adder = "Binaural";
          }
          channels.add(channel_adder);
          // Frequency
          if (!frequencies[channel_adder]) {
            frequencies[channel_adder] = new Set();
          }
          if ($scope.examType != "BHAFT") {
            frequencies[channel_adder].add(element.page.responseArea.examProperties.F / 1000);
          } else if ($scope.examType == "BHAFT") {
            frequencies[channel_adder].add(element.ThresholdFrequency ? round(element.ThresholdFrequency) : "-");
          }
        }
      });

      // Sort the channel and frequency
      $scope.channels = Array.from(channels);
      $scope.frequencies = frequencies;
      $scope.frequencies_combined = [];
      for (const [key, value] of Object.entries($scope.frequencies)) {
        $scope.frequencies[key] = Array.from(frequencies[key]);
        $scope.frequencies[key].sort(function(a, b) {
          return a - b;
        });
        for (const [key1, value1] of Object.entries($scope.frequencies[key])) {
          if (!$scope.frequencies_combined.includes(value1)) {
            $scope.frequencies_combined.push(value1);
          }
        }
      }
      $scope.frequencies_combined.sort(function(a, b) {
        return a - b;
      });

      // Sort the output channels left first so the table can iterate this array to create columns in correct order.
      let sortOrder = ["Left", "Right"];
      $scope.channels.sort(function(a, b) {
        return sortOrder.indexOf(a) - sortOrder.indexOf(b);
      });

      $scope.levels = {};
      $scope.svantek_results = {};
      $scope.slm_results = {};
      $scope.levels_combined = {};
      $scope.svantek_combined = {};
      $scope.slm_combined = {};
      $scope.svantek_combined["bandLevel"] = {};
      $scope.svantek_combined["leqA"] = {};
      $scope.slm_combined["bandLevel"] = {};
      $scope.slm_combined["leqA"] = {};
      // Loop through channels and frequencies to build the levels, svantek, slm, masking, and screen arrays
      $scope.channels.forEach(c => {
        $scope.levels[c] = [];
        $scope.svantek_results[c] = {};
        $scope.slm_results[c] = {};
        $scope.levels_combined[c] = {};
        $scope.svantek_results[c]["bandLevel"] = [];
        $scope.svantek_results[c]["leqA"] = [];
        $scope.slm_results[c]["bandLevel"] = [];
        $scope.slm_results[c]["leqA"] = [];
        $scope.svantek_combined["bandLevel"][c] = {};
        $scope.svantek_combined["leqA"][c] = {};
        $scope.slm_combined["bandLevel"][c] = {};
        $scope.slm_combined["leqA"][c] = {};
        if ($scope.masking) {
          $scope.maskingLevels[c] = [];
        }
        if ($scope.screening) {
          $scope.screens[c] = {};
          $scope.screenerThresholds.forEach(t => {
            $scope.screens[c][t] = [];
          });
        }
      });
      var ear;
      // Loop through channels and frequencies AND resultsList. If the relevant exam is found add to levels - this preserves order.
      $scope.channels.forEach(c => {
        $scope.frequencies_combined.forEach(f => {
          $scope.resultsList.forEach(e => {
            if (angular.isDefined(e.examProperties)) {
              if ($scope.screening) {
                if (e.L && !JSON.stringify(e.L).startsWith("[")) {
                  thresh = e.L;
                } else {
                  thresh = e.examProperties.Lstart;
                }
              }
              if (
                leftCodes.includes(e.examProperties.OutputChannel) &&
                !rightCodes.includes(e.examProperties.OutputChannel)
              ) {
                ear = "Left";
              } else if (
                rightCodes.includes(e.examProperties.OutputChannel) &&
                !leftCodes.includes(e.examProperties.OutputChannel)
              ) {
                ear = "Right";
              } else {
                ear = "Binaural";
              }
              if (c == ear && f == e.examProperties.F / 1000) {
                // Level
                if ($scope.examType != "BHAFT") {
                  $scope.levels[c].push(round(e.Threshold));
                  $scope.levels_combined[c][f] = round(e.Threshold);
                  if ($scope.showSvantek) {
                    $scope.svantek_combined["bandLevel"][c][f] = e.svantek.bandLevel
                      ? e.svantek.bandLevel.toPrecision(3)
                      : "-";
                    $scope.svantek_combined["leqA"][c][f] = e.svantek.LeqA ? e.svantek.LeqA.toPrecision(3) : "-";
                  }
                  if ($scope.showSLMNoise) {
                    $scope.slm_combined["bandLevel"][c][f] = e.slm.bandLevel ? e.slm.bandLevel.toPrecision(3) : "-";
                    $scope.slm_combined["leqA"][c][f] = e.slm.LeqA ? e.slm.LeqA.toPrecision(3) : "-";
                  }
                }
                // maskingLevel
                if ($scope.masking) {
                  if (e.MaskingLevel) {
                    $scope.maskingLevels[c].push(e.MaskingLevel);
                  } else if (e.FinalMaskingLevel) {
                    $scope.maskingLevels[c].push(e.FinalMaskingLevel);
                  }
                }
                // Screener
                if ($scope.screening) {
                  if (thresh) {
                    $scope.screens[c][thresh].push(parseScreenerResult(e.response));
                  }
                }
              } else {
                // Level
                if ($scope.examType != "BHAFT") {
                  if (!$scope.levels_combined[c][f]) {
                    $scope.levels_combined[c][f] = "-";
                  }
                  if ($scope.showSvantek) {
                    if (!$scope.svantek_combined["bandLevel"][c][f]) {
                      $scope.svantek_combined["bandLevel"][c][f] = "-";
                      $scope.svantek_combined["leqA"][c][f] = "-";
                    }
                  }
                  if ($scope.showSLMNoise) {
                    if (!$scope.slm_combined["bandLevel"][c][f]) {
                      $scope.slm_combined["bandLevel"][c][f] = "-";
                      $scope.slm_combined["leqA"][c][f] = "-";
                    }
                  }
                } else if ($scope.examType == "BHAFT") {
                  if (c == ear && f == round(e.ThresholdFrequency)) {
                    $scope.levels[c].push(e.ThresholdLevel ? round(e.ThresholdLevel) : "-");
                  }
                }
              }
              if ($scope.showSvantek) {
                if ($scope.svantek_results[c]["bandLevel"].length < $scope.levels[c].length) {
                  $scope.svantek_results[c]["bandLevel"].push(
                    e.svantek.bandLevel ? e.svantek.bandLevel.toPrecision(3) : "-"
                  );
                  $scope.svantek_results[c]["leqA"].push(e.svantek.LeqA ? e.svantek.LeqA.toPrecision(3) : "-");
                }
              }
              if ($scope.showSLMNoise) {
                if ($scope.slm_results[c]["bandLevel"].length < $scope.levels[c].length) {
                  $scope.slm_results[c]["bandLevel"].push(e.slm.bandLevel ? e.slm.bandLevel.toPrecision(3) : "-");
                  $scope.slm_results[c]["leqA"].push(e.slm.LeqA ? e.slm.LeqA.toPrecision(3) : "-");
                }
              }
            } // Not sure if this is even needed, but it was previously there
            else if (!angular.isDefined(e.examProperties) && angular.isDefined(e.page)) {
              if ($scope.screening) {
                if (
                  e.page.responseArea.examProperties &&
                  !JSON.stringify(e.page.responseArea.examProperties).startsWith("[")
                ) {
                  thresh = e.L;
                } else {
                  thresh = e.page.responseArea.examProperties.Lstart;
                }
              }
              if (
                leftCodes.includes(e.page.responseArea.examProperties.OutputChannel) &&
                !rightCodes.includes(e.page.responseArea.examProperties.OutputChannel)
              ) {
                ear = "Left";
              } else if (
                rightCodes.includes(e.page.responseArea.examProperties.OutputChannel) &&
                !leftCodes.includes(e.page.responseArea.examProperties.OutputChannel)
              ) {
                ear = "Right";
              } else {
                ear = "Binaural";
              }
              if (c == ear && f == e.page.responseArea.examProperties.F / 1000) {
                // Level
                if ($scope.examType != "BHAFT") {
                  $scope.levels[c].push(round(e.page.responseArea.Threshold));
                  $scope.levels_combined[c][f] = round(e.page.responseArea.Threshold);
                  if ($scope.showSvantek) {
                    $scope.svantek_combined["bandLevel"][c][f] = e.page.responseArea.svantek.bandLevel
                      ? e.svantek.bandLevel.toPrecision(3)
                      : "-";
                    $scope.svantek_combined["leqA"][c][f] = e.page.responseArea.svantek.LeqA
                      ? e.svantek.LeqA.toPrecision(3)
                      : "-";
                  }
                  if ($scope.showSLMNoise) {
                    $scope.slm_combined["bandLevel"][c][f] = e.page.responseArea.slm.bandLevel
                      ? e.slm.bandLevel.toPrecision(3)
                      : "-";
                    $scope.slm_combined["leqA"][c][f] = e.page.responseArea.slm.LeqA ? e.slm.LeqA.toPrecision(3) : "-";
                  }
                }
                // maskingLevel
                if ($scope.masking) {
                  if (e.MaskingLevel) {
                    $scope.maskingLevels[c].push(e.page.responseArea.MaskingLevel);
                  } else if (e.FinalMaskingLevel) {
                    $scope.maskingLevels[c].push(e.page.responseArea.FinalMaskingLevel);
                  }
                }
                // Screener
                if ($scope.screening) {
                  $scope.screens[c][thresh].push(parseScreenerResult(e.page.responseArea.response));
                }
              } else {
                // Level
                if ($scope.examType != "BHAFT") {
                  if (!$scope.levels_combined[c][f]) {
                    $scope.levels_combined[c][f] = "-";
                  }
                  if ($scope.showSvantek) {
                    if (!$scope.svantek_combined["bandLevel"][c][f]) {
                      $scope.svantek_combined["bandLevel"][c][f] = "-";
                      $scope.svantek_combined["leqA"][c][f] = "-";
                    }
                  }
                  if ($scope.showSLMNoise) {
                    if (!$scope.slm_combined["bandLevel"][c][f]) {
                      $scope.slm_combined["bandLevel"][c][f] = "-";
                      $scope.slm_combined["leqA"][c][f] = "-";
                    }
                  }
                } else if ($scope.examType == "BHAFT") {
                  if (c == ear && f == round(e.page.responseArea.ThresholdFrequency)) {
                    $scope.levels[c].push(
                      e.page.responseArea.ThresholdLevel ? round(e.page.responseArea.ThresholdLevel) : "-"
                    );
                  }
                }
              }
              if ($scope.showSvantek) {
                if ($scope.svantek_results[c]["bandLevel"].length < $scope.levels[c].length) {
                  $scope.svantek_results[c]["bandLevel"].push(
                    e.page.responseArea.svantek.bandLevel ? e.page.responseArea.svantek.bandLevel.toPrecision(3) : "-"
                  );
                  $scope.svantek_results[c]["leqA"].push(
                    e.page.responseArea.svantek.LeqA ? e.page.responseArea.svantek.LeqA.toPrecision(3) : "-"
                  );
                }
              }
              if ($scope.showSLMNoise) {
                if ($scope.slm_results[c]["bandLevel"].length < $scope.levels[c].length) {
                  $scope.slm_results[c]["bandLevel"].push(
                    e.page.responseArea.slm.bandLevel ? e.page.responseArea.slm.bandLevel.toPrecision(3) : "-"
                  );
                  $scope.slm_results[c]["leqA"].push(
                    e.page.responseArea.slm.LeqA ? e.page.responseArea.slm.LeqA.toPrecision(3) : "-"
                  );
                }
              }
            }
          });
        });
      });

      // Setup new object for screening exams 'Full' / 'Summary' table view
      $scope.screensFullThreshold = {};
      $scope.screensFullResult = {};
      if ($scope.screens) {
        for (const [key0, value0] of Object.entries($scope.screens)) {
          $scope.screensFullThreshold[key0] = [];
          $scope.screensFullResult[key0] = [];
          if (key0) {
            for (const [key1, value1] of Object.entries($scope.screens[key0])) {
              $scope.screens[key0][key1].forEach(e => {
                $scope.screensFullThreshold[key0].push(key1);
                $scope.screensFullResult[key0].push(e);
              });
            }
          }
        }
      }

      // Debugging
      // console.log("$scope.showSvantek", $scope.showSvantek);
      // console.log("$scope.showSLMNoise", $scope.showSLMNoise);
      // console.log("$scope.svantek_results", $scope.svantek_results);
      // console.log("$scope.svantek_combined", $scope.svantek_combined);
      // console.log("$scope.slm_results", $scope.slm_results);
      // console.log("$scope.slm_combined", $scope.slm_combined);
      // console.log("$scope.channels", $scope.channels);
      // console.log("$scope.frequencies", $scope.frequencies);
      // console.log("$scope.levels", $scope.levels);
      // console.log("$scope.levels_combined", $scope.levels_combined);
      // console.log("$scope.maskingLevels", $scope.maskingLevels);
      // console.log("$scope.screenerThresholds", $scope.screenerThresholds);
      // console.log("$scope.screens", $scope.screens);
      // console.log("$scope.screensFullThreshold", $scope.screensFullThreshold);
      // console.log("$scope.screensFullResult", $scope.screensFullResult);
      // console.log("$scope.masking", $scope.masking);
      // console.log("$scope.screening", $scope.screening);
    }
  });
