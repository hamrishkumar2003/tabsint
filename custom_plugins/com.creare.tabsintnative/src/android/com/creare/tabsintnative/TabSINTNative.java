package com.creare.tabsintnative;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;

import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaWebView;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.os.Build;
import android.os.Handler;
import android.webkit.WebView;

import android.util.Log;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.PluginResult;

// for Immersive view...
import android.view.View;
import android.view.WindowManager;

// for Volume buttons
import android.media.AudioManager;

// for intent send data to another app
import java.lang.reflect.Array;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.Date;
import java.util.TimeZone;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

//for handling throws IOException
import java.io.*;

import android.provider.MediaStore;
import android.provider.DocumentsContract;
import android.database.Cursor;
import android.content.ClipData;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.net.Uri;

// Usb imports
import android.hardware.usb.UsbManager;
import android.hardware.usb.UsbDevice;
import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import java.util.HashMap;

// Flic imports
// NOTE: commenting out flic functionality from 47937026b086a035e8acf764551cbfe54d6435b5
// Uncomment all changes from that commit to re-enable this, but for now it is preventing
// TabSINT from building for target Android API 30 (Android 11).
// Some more information about this problem here: https://gitlab.com/creare-com/tabsint/-/issues/598
// and here: https://gitlab.com/creare-com/tabsint/-/merge_requests/246
// import io.flic.flic2libandroid.Flic2Manager;
// import io.flic.flic2libandroid.Flic2ButtonListener;
// import io.flic.flic2libandroid.Flic2ScanCallback;
// import io.flic.flic2libandroid.Flic2Button;

import android.content.ContentResolver;
import android.webkit.MimeTypeMap;

import java.io.FileDescriptor;

/**
 * This calls out to the ZXing barcode reader and returns the result.
 *
 * @sa https://github.com/apache/cordova-android/blob/master/framework/src/org/apache/cordova/CordovaPlugin.java
 */
public class TabSINTNative extends CordovaPlugin {
    public static final String ACTION_INITIALIZE = "initialize";
    public static final String ACTION_RESET_AUDIO = "resetAudio";
    public static final String ACTION_SET_AUDIO = "setAudio";
    public static final String ACTION_GET_AUDIO_VOLUME = "getAudioVolume";
    public static final String ACTION_REGISTER_USB_DEVICE_LISTENER = "registerUsbDeviceListener";
    public static final String ACTION_UNREGISTER_USB_DEVICE_LISTENER = "unregisterUsbDeviceListener";

    private CallbackContext volumeCallbackContext = null;

    // Usb device related variables
    private UsbManager mUsbManager;
    private UsbDevice mUsbDevice;
    private boolean mUsbConnected = false;
    private BroadcastReceiver mUsbReceiver;
    private CallbackContext mUsbDeviceCallbackContext = null;
    private CordovaInterface mCordova;
    private IntentFilter mFilter;

    private AudioManager mAudioManager;

    // Flic variables
    private boolean isScanning;

    private static final String TAG = "TabsintNativePlugin";

    /**
     * Constructor.
     */
    public TabSINTNative() {
        Log.d(TAG, "in Tabsint Native constructor");
    }

    /**
     * Sets the context of the Command. This can then be used to do things like
     * get file paths associated with the Activity.
     *
     * @param cordova The context of the main Activity.
     * @param webView The CordovaWebView Cordova is running in.
     */
    @Override
    public void initialize(CordovaInterface cordova, CordovaWebView webView) {
    Log.d(TAG, "in Tabsint Native initialize");
        super.initialize(cordova, webView);

        mUsbManager = (UsbManager) cordova.getActivity().getSystemService(Context.USB_SERVICE);
        mCordova = cordova;
        mFilter = new IntentFilter();
        mFilter.addAction(UsbManager.ACTION_USB_DEVICE_DETACHED);
        mFilter.addAction(UsbManager.ACTION_USB_DEVICE_ATTACHED);
        mUsbReceiver = new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                if (UsbManager.ACTION_USB_DEVICE_DETACHED.equals(action)) {
                    mUsbDevice = null;
                    sendUsbDeviceInfo();
                } else if (UsbManager.ACTION_USB_DEVICE_ATTACHED.equals(action)) {
                    mUsbDevice = (UsbDevice) intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
                    sendUsbDeviceInfo();
                }
            }
        };
        mAudioManager = (AudioManager) this.cordova.getActivity().getSystemService(Context.AUDIO_SERVICE);
    }

    /**
     * Executes the request.
     *
     * This method is called from the WebView thread. To do a non-trivial amount of
     * work, use:
     * cordova.getThreadPool().execute(runnable);
     *
     * To run on the UI thread, use:
     * cordova.getActivity().runOnUiThread(runnable);
     *
     * @param action          The action to execute.
     * @param args            The exec() arguments.
     * @param callbackContext The callback context used when calling back into
     *                        JavaScript.
     * @return Whether the action was valid.
     *
     * @sa https://github.com/apache/cordova-android/blob/master/framework/src/org/apache/cordova/CordovaPlugin.java
     */
    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {

        Log.d(TAG, "in Tabsint Native execute" + action);

        if (action.equals(ACTION_INITIALIZE) || action.equals(ACTION_RESET_AUDIO) || action.equals(ACTION_SET_AUDIO)) {
            int volume = action.equals(ACTION_SET_AUDIO) ? args.getInt(0) : 100;
            setAudio(volume);
            if (getAudioVolume() != volume) {
                JSONObject r = new JSONObject();
                r.put("error", false);
                r.put("code", 99);
                r.put("volume", getAudioVolume());
                r.put("message", "Volume not properly set.");
                callbackContext.error(r);
                return false;
            } else {
                callbackContext.success();
            }
            return true;
        } else if (action.equals(ACTION_GET_AUDIO_VOLUME)) {
            JSONObject r = new JSONObject();
            r.put("success", true);
            r.put("volume", getAudioVolume());
            callbackContext.success(r);
            return true;
        } else if (action.equals(ACTION_REGISTER_USB_DEVICE_LISTENER)) {
            return registerUsbDeviceListener(callbackContext);
        } else if (action.equals(ACTION_UNREGISTER_USB_DEVICE_LISTENER)) {
            return unregisterUsbDeviceListener(callbackContext);
        } else {
            callbackContext.error("Invalid action");
            return false;
        }
    }

    public void setAudio(int volume) {
        int maxVolume = mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
        int sysVolume = Math.round((float) (volume * (float) maxVolume / 100));
        if (sysVolume > maxVolume) {
            sysVolume = maxVolume;
        }
        mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, sysVolume, 0);
    }

    public int getAudioVolume() {
        int maxVolume = mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
        int currSystemVol = mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        int volLevel = Math.round((float) (currSystemVol * 100.0 / (float) maxVolume));
        return volLevel;
    }

    /**
     * Send the latest device info to the app.
     */
    private void sendUsbDeviceInfo() {
        Log.d(TAG, "in Tabsint Native sendUsbDeviceInfo");
        if (mUsbDeviceCallbackContext != null) {
            mUsbConnected = mUsbDevice != null && mUsbDevice.getVendorId() == 11799
                    && mUsbDevice.getProductId() == 40963;
            PluginResult result;
            JSONObject usbJSON = new JSONObject();
            try {
                usbJSON.put("isUsbConnected", mUsbConnected);
                usbJSON.put("mUsbDevice", mUsbDevice != null ? mUsbDevice.getManufacturerName() : "undefined");
            } catch (Exception e) {
                result = new PluginResult(PluginResult.Status.ERROR,
                        "TabSINTNative.sendUsbDeviceInfo error: " + e.getMessage());
                result.setKeepCallback(true);
                mUsbDeviceCallbackContext.sendPluginResult(result);
            }
            result = new PluginResult(PluginResult.Status.OK, usbJSON);
            result.setKeepCallback(true);
            mUsbDeviceCallbackContext.sendPluginResult(result);
        }

    }

    private boolean unregisterUsbDeviceListener(CallbackContext callbackContext) {
        if (mUsbDeviceCallbackContext != null) {
            mUsbDevice = null;
            mCordova.getActivity().getApplicationContext().unregisterReceiver(mUsbReceiver);
            mUsbDeviceCallbackContext
                    .sendPluginResult(new PluginResult(PluginResult.Status.OK, "This context is being removed."));
            callbackContext
                    .sendPluginResult(new PluginResult(PluginResult.Status.OK, "Success: Removed UsbDeviceReceiver"));
            mUsbDeviceCallbackContext = null;
            return true;
        }
        callbackContext.sendPluginResult(
                new PluginResult(PluginResult.Status.ERROR, "There is no UsbDeviceReceiver to remove."));
        return false;
    }

    private boolean registerUsbDeviceListener(final CallbackContext callbackContext) {
        if (mUsbDeviceCallbackContext == null) {
            mUsbDeviceCallbackContext = callbackContext;
            mCordova.getActivity().getApplicationContext().registerReceiver(mUsbReceiver, mFilter);

            // Check for current USB device
            mUsbDevice = null;
            HashMap<String, UsbDevice> usbDevices = mUsbManager.getDeviceList();
            if (usbDevices.values().size() > 0) {
                for (UsbDevice d : usbDevices.values()) {
                    mUsbDevice = d;
                    break;
                }
            }
            sendUsbDeviceInfo();
            return true;
        }
        mUsbDeviceCallbackContext
                .sendPluginResult(new PluginResult(PluginResult.Status.ERROR, "UsbDeviceReceiver already registered."));
        return false;

    }
}
